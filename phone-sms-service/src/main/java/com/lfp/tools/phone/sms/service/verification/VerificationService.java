package com.lfp.tools.phone.sms.service.verification;

import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.threads.Threads;

import org.threadly.concurrent.future.FutureUtils;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.neovisionaries.i18n.CountryCode;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface VerificationService {

	Flux<String> getServices();

	Mono<String> getServiceForName(String name);

	Flux<CountryCode> getCountryCodes();

	Flux<Entry<CountryCode, List<ServiceStats>>> getServiceStats(CountryCode... countryCodeFilters);

	Flux<VerificationUpdate> getVerification(String service, CountryCode countryCode);

	default Context getVerificationContext(String service, CountryCode countryCode) {
		var flux = getVerification(service, countryCode);
		return new Context(flux);
	}

	public static class Context extends Scrapable.Impl {

		private final SettableListenableFuture<PhoneNumber> phonenumberFuture = new SettableListenableFuture<>(false);
		private final SettableListenableFuture<String> messageFuture = new SettableListenableFuture<>(false);
		private VerificationUpdate lastUpdate;

		public Context(Flux<VerificationUpdate> updateFlux) {
			updateFlux = Objects.requireNonNull(updateFlux);
			this.onScrap(() -> {
				Threads.Futures.cancel(this.phonenumberFuture, true);
				Threads.Futures.cancel(this.messageFuture, true);
			});
			var subscription = updateFlux.subscribe(update -> {
				this.lastUpdate = update;
				if (update.getPhoneNumber().isPresent())
					this.phonenumberFuture.setResult(update.getPhoneNumber().get());
				if (update.getMessage().isPresent())
					this.messageFuture.setResult(update.getMessage().get());
				if (update.isComplete())
					this.close();
			}, error -> {
				phonenumberFuture.setFailure(error);
				messageFuture.setFailure(error);
			}, () -> {
				phonenumberFuture.setResult(null);
				messageFuture.setResult(null);
			});
			FutureUtils.makeCompleteFuture(this.phonenumberFuture, this.messageFuture).listener(subscription::dispose);
		}

		public ListenableFuture<PhoneNumber> getPhoneNumberFuture() {
			return this.phonenumberFuture;
		}

		public ListenableFuture<String> getMessageFuture() {
			return this.messageFuture;
		}

		public Optional<VerificationUpdate> getLastUpdate() {
			return Optional.ofNullable(this.lastUpdate);
		}

	}
}
