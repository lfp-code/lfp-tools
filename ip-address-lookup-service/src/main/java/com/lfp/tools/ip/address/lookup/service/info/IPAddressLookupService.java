package com.lfp.tools.ip.address.lookup.service.info;

import java.io.IOException;

import com.lfp.joe.net.http.ip.IPAddressInfo;

public interface IPAddressLookupService {

	String getIp(String lookup) throws IOException;

	IPAddressInfo getInfo(String lookup) throws IOException;
}
