package com.lfp.tools.ip.address.lookup.service.proxy;

import java.io.IOException;

import com.lfp.joe.net.proxy.Proxy;

public interface ProxyIPAddressLookupService {

	String getPublicIP(Proxy proxy) throws IOException;

}
