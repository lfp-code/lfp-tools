package com.lfp.tools.robots.txt.lookup.service;

import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.utils.Utils;

public interface RobotsTxtLookupService {

	default RobotsTxtRecord apply(String lookup) {
		return apply(lookup, null);
	}

	default RobotsTxtRecord apply(String lookup, Proxy proxy) {
		if (Utils.Strings.isBlank(lookup))
			return null;
		return apply(RobotsTxtLookupRequest.builder().lookup(lookup).proxy(proxy).build());
	}

	RobotsTxtRecord apply(RobotsTxtLookupRequest request);

}
