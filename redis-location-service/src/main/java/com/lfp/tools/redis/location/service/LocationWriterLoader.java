package com.lfp.tools.redis.location.service;

import java.time.Duration;
import java.util.Objects;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.tools.cache.RedissonWriterLoader;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.location.Location;
import com.lfp.joe.location.search.LocationService;

public abstract class LocationWriterLoader<S> extends RedissonWriterLoader<S, StatValue<Location>> {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private final Duration timeToLive;
	private LocationService<S> delegate;

	public LocationWriterLoader(RedissonClientLFP client, Duration timeToLive, LocationService<S> delegate) {
		super(client, THIS_CLASS, VERSION, CoreReflections.getNamedClassType(delegate.getClass()));
		this.timeToLive = timeToLive;
		this.delegate = Objects.requireNonNull(delegate);
	}

	@Override
	protected @Nullable StatValue<Location> loadFresh(@NonNull S key) throws Exception {
		var resultOp = delegate.mapToLocation(key);
		return StatValue.build(resultOp.orElse(null));
	}

	@Override
	protected @Nullable Duration getStorageTTL(@NonNull S key, @NonNull StatValue<Location> result) {
		return this.timeToLive;
	}

}
