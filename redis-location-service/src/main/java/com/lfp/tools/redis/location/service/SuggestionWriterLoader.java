package com.lfp.tools.redis.location.service;

import java.time.Duration;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.tools.cache.RedissonWriterLoader;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.location.search.LocationService;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.redis.location.service.SuggestionWriterLoader.CachedSuggestions;

public class SuggestionWriterLoader<S> extends RedissonWriterLoader<Entry<String, Integer>, CachedSuggestions<S>> {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 1;
	private final Duration timeToLive;
	private LocationService<S> delegate;

	public SuggestionWriterLoader(RedissonClientLFP client, Duration timeToLive, LocationService<S> delegate) {
		super(client, THIS_CLASS, VERSION, CoreReflections.getNamedClassType(delegate.getClass()));
		this.timeToLive = timeToLive;
		this.delegate = Objects.requireNonNull(delegate);
	}

	@Override
	protected @Nullable Iterable<Object> getKeyParts(@NonNull Entry<String, Integer> key) {
		String query = key.getKey();
		query = Utils.Strings.lowerCase(query);
		query = Utils.Strings.trim(query);
		if (Utils.Strings.isBlank(query))
			return null;
		Integer limit = key.getValue();
		if (limit == null)
			limit = -1;
		if (limit == 0 || limit < -1)
			return null;
		return Arrays.asList(query, limit);
	}

	@Override
	protected @Nullable CachedSuggestions<S> loadFresh(@NonNull Entry<String, Integer> key) throws Exception {
		var result = new CachedSuggestions<S>();
		for (var value : delegate.search(key.getKey(), key.getValue()).nonNull()) {
			result.add(value);
		}
		return result;
	}

	@Override
	protected @Nullable ThrowingFunction<ThrowingSupplier<CachedSuggestions<S>, Exception>, CachedSuggestions<S>, Exception> getStorageLockAccessor(
			@NonNull Entry<String, Integer> key, String lockKey) {
		return null;
	}

	@Override
	protected @Nullable Duration getStorageTTL(@NonNull Entry<String, Integer> key,
			@NonNull CachedSuggestions<S> result) {
		return this.timeToLive;
	}

	public static class CachedSuggestions<S> implements Iterable<S> {

		private final Set<StatValue<S>> values = new LinkedHashSet<>();

		public boolean add(S suggestion) {
			if (suggestion == null)
				return false;
			return values.add(StatValue.build(suggestion));
		}

		@Override
		public Iterator<S> iterator() {
			return Utils.Lots.stream(values).nonNull().map(StatValue::getValue).nonNull().iterator();
		}

	}
}
