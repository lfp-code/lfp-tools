package com.lfp.tools.redis.location.service;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.base.Stopwatch;
import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.location.Location;
import com.lfp.joe.location.search.AbstractLocationService;
import com.lfp.joe.location.search.LocationService;
import com.lfp.joe.location.search.bing.BingMapsLocationSearchService;
import com.lfp.joe.location.search.bing.Suggestion;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.tools.redis.location.service.SuggestionWriterLoader.CachedSuggestions;

import one.util.streamex.StreamEx;

public class RedisLocationService<S> extends AbstractLocationService<S> {

	public static RedisLocationService<Suggestion> DEFAULT_INSTANCE = new RedisLocationService<>(
			BingMapsLocationSearchService.getDefault(), TendisClients.getDefault(), 50_000, Duration.ofHours(1),
			TendisClients.getDefault(), 25_000, Duration.ofDays(7), null, Duration.ofSeconds(15));

	private LoadingCache<Entry<String, Integer>, CachedSuggestions<S>> suggestionCache;
	private LoadingCache<S, StatValue<Location>> locationCache;

	public RedisLocationService(LocationService<S> delegate, RedissonClientLFP suggestionClient,
			long memoryMaximumSizeSuggestion, Duration suggestionTTL, RedissonClientLFP locationClient,
			long memoryMaximumSizeLocation, Duration locationTTL, Duration memoryExpireAfterWrite,
			Duration memoryExpireAfterAccess) {
		this.suggestionCache = new SuggestionWriterLoader<>(suggestionClient, suggestionTTL, delegate).buildCache(Caches
				.newCaffeineBuilder(memoryMaximumSizeSuggestion, memoryExpireAfterWrite, memoryExpireAfterAccess));
		this.locationCache = new LocationWriterLoader<S>(locationClient, locationTTL, delegate) {

			@Override
			protected @Nullable Iterable<Object> getKeyParts(@NonNull S key) {
				return RedisLocationService.this.getKeyParts(key);
			}

			@Override
			protected @Nullable ThrowingFunction<ThrowingSupplier<StatValue<Location>, Exception>, StatValue<Location>, Exception> getStorageLockAccessor(
					@NonNull S key, String lockKey) {
				return createStorageLockAccessorDefault(null, lockKey);
			}
		}.buildCache(
				Caches.newCaffeineBuilder(memoryMaximumSizeLocation, memoryExpireAfterWrite, memoryExpireAfterAccess));
	}

	@Override
	protected Iterable<S> searchInternal(String input, int limit) throws IOException, InterruptedException {
		var cachedSuggestions = this.suggestionCache.get(Utils.Lots.entry(input, limit));
		return cachedSuggestions;
	}

	@Override
	protected StreamEx<Location> mapToLocationInternal(StreamEx<S> searchResults)
			throws IOException, InterruptedException {
		return searchResults.map(v -> {
			var statValue = this.locationCache.get(v);
			if (statValue == null)
				return null;
			return statValue.getValue();
		});
	}

	protected Iterable<Object> getKeyParts(@NonNull S key) {
		Object result;
		if (key instanceof Hashable)
			result = ((Hashable) key).hash().encodeHex();
		else {
			var je = Serials.Gsons.get().toJsonTree(key);
			result = Serials.Gsons.deepHash(je).encodeHex();
		}
		return List.of(result);
	}

	public static void main(String[] args) throws IOException {
		Suggestion suggestion = null;
		try (Scanner scanner = new Scanner(System.in);) {
			while (true) {
				System.out.println("Enter query");
				var line = scanner.nextLine();
				if (Utils.Strings.isBlank(line))
					break;
				var sw = Stopwatch.createStarted();
				var results = RedisLocationService.DEFAULT_INSTANCE.search(line, 10).toList();
				var first = Utils.Lots.stream(results).findFirst();
				if (first.isPresent())
					suggestion = first.get();
				System.out.println(Serials.Gsons.getPretty().toJson(results));
				System.out.println(sw.elapsed(TimeUnit.MILLISECONDS));
			}
		}
		var sw = Stopwatch.createStarted();
		var location = RedisLocationService.DEFAULT_INSTANCE.mapToLocation(suggestion).orElse(null);
		System.out.println(Serials.Gsons.getPretty().toJson(location));
		System.out.println(sw.elapsed(TimeUnit.MILLISECONDS));
	}

}
