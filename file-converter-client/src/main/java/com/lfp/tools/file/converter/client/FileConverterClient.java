package com.lfp.tools.file.converter.client;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.lfp.joe.net.media.MediaTypes;
import com.lfp.joe.retrofit.client.RFit;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.InputStreamSource;
import com.lfp.tools.file.converter.service.FileConverterService;
import com.lfp.tools.file.converter.service.GotenbergService;
import com.lfp.tools.file.converter.service.GotenbergService.ConversionSource;
import com.lfp.tools.file.converter.service.OutputFileType;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.Validate;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

import com.google.common.net.MediaType;

import okhttp3.MultipartBody;

public class FileConverterClient implements FileConverterService {

	public static final FileConverterClient INSTANCE = new FileConverterClient();

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final int VERSION = 0;
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final float IMAGE_DPI = 300;
	private static final String GOTENBERG_PARAMETER_NAME = "file";
	private static final String GOTENBERG_FILE_NAME = "index";

	@Override
	public InputStream convert(InputStreamSource source, MediaType mediaType, OutputFileType outputFileType)
			throws IOException {
		Objects.requireNonNull(source);
		Objects.requireNonNull(outputFileType);
		mediaType = normalizeMediaType(source, mediaType);
		if (Objects.equals(mediaType, outputFileType.getMediaType()))
			return source.openStream();
		if (outputFileType.isImage() && isImage(mediaType))
			return convertImageToImage(source, outputFileType);
		if (outputFileType.isImage() && isPDF(mediaType))
			return convertPDFToImage(source, outputFileType);
		Set<Closeable> closeTracking = Utils.Lots.newConcurrentHashSet();
		try {
			return convert(source, mediaType, outputFileType, closeTracking);
		} finally {
			for (var closeable : closeTracking)
				closeable.close();
		}
	}

	private InputStream convert(InputStreamSource source, MediaType mediaType, OutputFileType outputFileType,
			Set<Closeable> closeTracking) throws FileNotFoundException, IOException {
		if (outputFileType.isImage()) {
			var pdfFile = tempFile();
			closeTracking.add(pdfFile::delete);
			try (var pdfInputStream = convert(source, mediaType, OutputFileType.PDF);
					var fos = new FileOutputStream(pdfFile)) {
				Utils.Bits.copy(pdfInputStream, fos);
			}
			return convert(InputStreamSource.create(pdfFile), MediaType.PDF, outputFileType);
		}
		var conversionSource = ConversionSource.tryParse(source, mediaType).orElse(null);
		Validate.isTrue(conversionSource != null && OutputFileType.PDF.equals(outputFileType),
				"unable to convert from:%s to:%s", mediaType, outputFileType);
		List<MultipartBody.Part> parts = buildParts(source, mediaType, conversionSource, closeTracking);
		var call = RFit.Clients.get(GotenbergService.class).convert(conversionSource, parts);
		var responseBody = RFit.Calls.consume(call);
		return responseBody.byteStream();
	}

	private List<MultipartBody.Part> buildParts(InputStreamSource source, MediaType mediaType,
			ConversionSource conversionSource, Set<Closeable> closeTracking) throws IOException {
		List<MultipartBody.Part> parts = new ArrayList<>();
		if (ConversionSource.Markdown.equals(conversionSource)) {
			File markdownHtmlFile = Utils.Resources.getResourceFile("markdown.html");
			parts.add(RFit.Calls.createFilePart(GOTENBERG_PARAMETER_NAME, markdownHtmlFile,
					GOTENBERG_FILE_NAME + ".html"));
		}
		File sourceFile = tempFile();
		closeTracking.add(sourceFile::delete);
		try (var is = source.openStream(); var fos = new FileOutputStream(sourceFile)) {
			Utils.Bits.copy(is, fos);
		}
		String ext = MediaTypes.getExtension(mediaType).orElse("bin");
		parts.add(RFit.Calls.createFilePart(GOTENBERG_PARAMETER_NAME, sourceFile, GOTENBERG_FILE_NAME + "." + ext));
		return parts;
	}

	private static MediaType normalizeMediaType(InputStreamSource source, MediaType mediaType) {
		if (mediaType != null)
			return mediaType;
		return MediaTypes.tryParse(source).orElse(MediaType.APPLICATION_BINARY);
	}

	private static InputStream convertImageToImage(InputStreamSource source, OutputFileType outputFileType)
			throws IOException {
		BufferedImage image = ImageIO.read(source.openStream());
		BufferedImage result = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
		result.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);
		try (var baos = new ByteArrayOutputStream();) {
			ImageIO.write(result, outputFileType.toString(), baos);
			return new ByteArrayInputStream(baos.toByteArray());
		}
	}

	private static InputStream convertPDFToImage(InputStreamSource source, OutputFileType outputFileType)
			throws IOException {
		try (PDDocument pdDocument = PDDocument.load(source.openStream()); var baos = new ByteArrayOutputStream()) {
			// Create the renderer
			PDFRenderer pdfRenderer = new PDFRenderer(pdDocument);
			BufferedImage joinBufferedImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
			for (int x = 0; x < pdDocument.getNumberOfPages(); x++) {
				BufferedImage bImage = pdfRenderer.renderImageWithDPI(x, IMAGE_DPI, ImageType.RGB);
				joinBufferedImage = joinBufferedImage(joinBufferedImage, bImage);
			}
			ImageIO.write(joinBufferedImage, outputFileType.toString(), baos);
			return new ByteArrayInputStream(baos.toByteArray());
		}
	}

	private static BufferedImage joinBufferedImage(BufferedImage img1, BufferedImage img2) {
		int width = Math.max(img1.getWidth(), img2.getWidth());
		int height = img1.getHeight() + img2.getHeight();
		// create a new buffer and draw two image into the new image
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = newImage.createGraphics();
		Color oldColor = g2.getColor();
		// fill background
		g2.setPaint(Color.WHITE);
		g2.fillRect(0, 0, width, height);
		// draw image
		g2.setColor(oldColor);
		g2.drawImage(img1, null, 0, 0);
		g2.drawImage(img2, null, 0, img1.getHeight());
		g2.dispose();
		return newImage;
	}

	private static boolean isImage(MediaType mediaType) {
		if (mediaType != null && "image".equalsIgnoreCase(mediaType.type()))
			return true;
		return false;
	}

	private boolean isPDF(MediaType mediaType) {
		var ext = MediaTypes.getExtension(mediaType).orElse(null);
		if (Utils.Strings.equalsIgnoreCase(ext, "pdf"))
			return true;
		return false;
	}

	private static File tempFile() {
		return Utils.Files.tempFile(THIS_CLASS, VERSION, Utils.Crypto.getSecureRandomString() + ".bin");
	}

}
