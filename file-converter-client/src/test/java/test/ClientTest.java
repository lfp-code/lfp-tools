package test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;

import com.lfp.joe.net.media.MediaTypes;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.file.converter.client.FileConverterClient;
import com.lfp.tools.file.converter.service.OutputFileType;

public class ClientTest {

	public static void main(String[] args) throws IOException {

		System.out.println(MediaTypes.tryParse(new File("C:\\Users\\reggi\\Desktop\\captcha.html")).get());
		System.out.println(MediaTypes.tryParse(new File("C:\\Users\\reggi\\Desktop\\test.md")).get());
		System.out.println(MediaTypes.tryParse(new File("C:\\Users\\reggi\\Desktop\\test123.docx")).get());
		var file = new File("C:\\Users\\reggi\\Desktop\\captcha.html");
		var outputFileType = OutputFileType.PDF;
		File outFile;
		{
			var ext = MediaTypes.getExtension(outputFileType.getMediaType()).get();
			outFile = new File("temp/" + Utils.Crypto.getSecureRandomString() + "." + ext);
			outFile.getParentFile().mkdirs();
		}
		try (var is = FileConverterClient.INSTANCE.convert(file, outputFileType);
				var fos = new FileOutputStream(outFile)) {
			Utils.Bits.copy(is, fos);
		}
		System.out.println(outFile.getAbsolutePath());
	}

}
