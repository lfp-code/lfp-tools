package com.lfp.tools.http.probe.service;

import java.io.IOException;
import java.util.Objects;
import java.util.function.Consumer;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.browser.webdriver.client.Browsers;
import com.lfp.browser.webdriver.client.driver.WebDriverLFP;
import com.lfp.browser.webdriver.client.driver.chrome.ChromeOptionsLFP;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.RateLimiter;
import com.lfp.joe.core.function.Throws.ThrowingFunction;

import okhttp3.Interceptor.Chain;
import okhttp3.Request;
import okhttp3.Response;

public abstract class AbstractHttpProbeService<SD extends SessionData> implements HttpProbeService {

	private final HttpProbeServiceOptions options;
	private final LoadingCache<HttpProbeContext, SD> sessionDataCache;

	public AbstractHttpProbeService(HttpProbeServiceOptions options) {
		this.options = Objects.requireNonNull(options);
		this.sessionDataCache = createSessionDataCache(options);
	}

	public HttpProbeServiceOptions getOptions() {
		return options;
	}

	protected SD getSessionData(HttpProbeContext httpProbeContext) {
		if (this.sessionDataCache == null)
			return null;
		return this.sessionDataCache.get(httpProbeContext);
	}

	protected void invalidateSessionData(HttpProbeContext httpProbeContext) {
		if (httpProbeContext == null)
			return;
		if (this.sessionDataCache == null)
			return;
		var value = this.sessionDataCache.getIfPresent(httpProbeContext);
		if (value == null)
			return;
		this.sessionDataCache.invalidate(httpProbeContext);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected Response intercept(HttpProbeContext httpProbeContext, Chain chain) throws IOException {
		var request = chain.request();
		if (shouldAppendSessionData(httpProbeContext, request)) {
			var sessionData = this.getSessionData(httpProbeContext);
			if (sessionData != null) {
				request = sessionData.modifyRequest(request);
				if (request == null)
					return null;
				request = request.newBuilder().tag((Class) sessionData.getClass(), sessionData).build();
			}
		}
		if (shouldRateLimit(httpProbeContext, request))
			try {
				httpProbeContext.getRateLimiter().acquire();
			} catch (InterruptedException e) {
				throw Utils.Exceptions.as(e, IOException.class);
			}
		return chain.proceed(request);
	}

	protected <X> X webDriverAccess(HttpProbeContext httpProbeContext, Consumer<ChromeOptionsLFP> optionsSetup,
			ThrowingFunction<WebDriverLFP, X, IOException> accessor) throws IOException {
		var chromeOptions = createChromeOptions(httpProbeContext);
		if (optionsSetup != null)
			optionsSetup.accept(chromeOptions);
		var webdriverAccessSemaphore = this.getOptions().getWebdriverAccessSemaphore();
		if (webdriverAccessSemaphore != null)
			try {
				webdriverAccessSemaphore.acquire();
			} catch (InterruptedException e) {
				throw Utils.Exceptions.as(e, IOException.class);
			}
		var webDriver = createWebDriver(chromeOptions);
		try {
			return accessor.apply(webDriver);
		} finally {
			try {
				if (!webDriver.isClosing())
					webDriver.close();
			} finally {
				if (webdriverAccessSemaphore != null)
					webdriverAccessSemaphore.release();
			}
		}
	}

	protected ChromeOptionsLFP createChromeOptions(HttpProbeContext httpProbeContext) {
		var result = new ChromeOptionsLFP(httpProbeContext.getProxy().orElse(null));
		return result;
	}

	protected WebDriverLFP createWebDriver(ChromeOptionsLFP chromeOptions) {
		return Browsers.Drivers.chrome(chromeOptions);
	}

	@Override
	public HttpProbeContext apply(Proxy proxy) {
		return new AbstractHttpProbeContext<SD>(proxy) {

			@Override
			public String uuid() {
				return AbstractHttpProbeService.this.getUUID(this);
			}

			@Override
			protected String lookupProxyPublicIPAddress() throws IOException {
				return AbstractHttpProbeService.this.lookupProxyPublicIPAddress(this.getProxy().get());
			}

			@Override
			protected String getUserAgent() {
				return AbstractHttpProbeService.this.getUserAgent(this);
			}

			@Override
			public <X> X webDriverAccess(Consumer<ChromeOptionsLFP> optionsSetup,
					ThrowingFunction<WebDriverLFP, X, IOException> accessor) throws IOException {
				return AbstractHttpProbeService.this.webDriverAccess(this, optionsSetup, accessor);
			}

			@Override
			protected SD getSessionData() {
				return AbstractHttpProbeService.this.getSessionData(this);
			}

			@Override
			protected Response intercept(Chain chain) throws IOException {
				return AbstractHttpProbeService.this.intercept(this, chain);
			}

			@Override
			public RateLimiter getRateLimiter() {
				var rateLimiter = AbstractHttpProbeService.this.getRateLimiter(this);
				if (rateLimiter == null)
					return RateLimiter.NOOP;
				return rateLimiter;
			}

		};
	}

	protected abstract LoadingCache<HttpProbeContext, SD> createSessionDataCache(HttpProbeServiceOptions options);

	protected abstract String getUUID(HttpProbeContext httpProbeContext);

	protected abstract String lookupProxyPublicIPAddress(Proxy proxy) throws IOException;

	protected abstract String getUserAgent(HttpProbeContext httpProbeContext);

	protected abstract boolean shouldRateLimit(HttpProbeContext httpProbeContext, Request request);

	protected abstract boolean shouldAppendSessionData(HttpProbeContext httpProbeContext, Request request);

	protected abstract RateLimiter getRateLimiter(@NonNull HttpProbeContext httpProbeContext);

}
