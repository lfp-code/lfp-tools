package com.lfp.tools.http.probe.service;

import java.util.Date;

import com.lfp.joe.net.cookie.CookieStoreLFP;

import okhttp3.Request;

public interface SessionData {

	String getInstanceId();

	Date getExpiresAt();

	void setExpiresAt(Date expiresAt);

	CookieStoreLFP getCookieStore();

	Request modifyRequest(Request request);
}
