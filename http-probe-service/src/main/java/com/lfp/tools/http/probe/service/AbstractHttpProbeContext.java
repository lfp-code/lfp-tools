package com.lfp.tools.http.probe.service;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.net.cookie.CookieStoreLFP;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.cookie.CookieStoreJar;
import com.lfp.joe.okhttp.interceptor.MetaRefreshRedirectInterceptor;
import com.lfp.joe.okhttp.interceptor.UserAgentInterceptor;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.PropertyCache;

import at.favre.lib.bytes.Bytes;
import okhttp3.Interceptor.Chain;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public abstract class AbstractHttpProbeContext<SD extends SessionData> extends Scrapable.Impl
		implements HttpProbeContext {

	private final PropertyCache propertyCache = PropertyCache.create();
	private final MemoizedSupplier<Bytes> hashSupplier = Utils.Functions.memoize(() -> Utils.Crypto.hashMD5(uuid()));
	private final Proxy proxy;

	public AbstractHttpProbeContext(Proxy proxy) {
		this.proxy = proxy;
	}

	@Override
	public Bytes hash() {
		return hashSupplier.get();
	}

	@Override
	public Optional<String> getProxyPublicIPAddress() {
		if (this.getProxy().isEmpty())
			return Optional.empty();
		String ipAddress;
		try {
			ipAddress = this.propertyCache.get("getProxyPublicIPAddress", String.class, () -> {
				var result = lookupProxyPublicIPAddress();
				Validate.isTrue(IPs.isValidIpAddress(result));
				return result;
			});
		} catch (IOException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
		return Optional.of(ipAddress);
	}

	@Override
	public Optional<Proxy> getProxy() {
		return Optional.ofNullable(proxy);
	}

	@Override
	public OkHttpClient getClient() {
		return this.propertyCache.get("getClient", OkHttpClient.class, () -> {
			return createClient();
		});
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Map<String, Object> getAttributes() {
		return this.propertyCache.get("getAttributes", Map.class, () -> {
			return new ConcurrentHashMap();
		});
	}

	protected OkHttpClient createClient() {
		var cb = Ok.Clients.get(this.getProxy().orElse(null)).newBuilder();
		Supplier<CookieStoreLFP> cookieStoreSupplier = () -> {
			var sessionData = getSessionData();
			if (sessionData == null)
				return null;
			var cookieStore = sessionData.getCookieStore();
			return cookieStore;
		};
		cb.cookieJar(new CookieStoreJar() {

			@Override
			public CookieStoreLFP getCookieStore() {
				return cookieStoreSupplier.get();
			}
		});
		cb = cb.addInterceptor(new UserAgentInterceptor(() -> getUserAgent()));
		cb.addInterceptor(new MetaRefreshRedirectInterceptor());
		cb.addInterceptor(chain -> {
			var response = intercept(chain);
			if (response != null)
				return response;
			var mediaType = MediaType.parse("text/plain");
			var responseBody = Ok.Calls.createResponseBody(mediaType, Bytes.from("intercepted response is null"));
			return Ok.Calls.createResponse(chain.request(), 404, responseBody);
		});
		return cb.build();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		var hash = hash();
		result = prime * result + ((hash == null) ? 0 : hash.hashCode());
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractHttpProbeContext other = (AbstractHttpProbeContext) obj;
		var hash = this.hash();
		Supplier<Bytes> otherHash = () -> other.hash();
		if (hash == null) {
			if (otherHash.get() != null)
				return false;
		} else if (!hash.equals(otherHash.get()))
			return false;
		return true;
	}

	protected abstract String lookupProxyPublicIPAddress() throws IOException;

	protected abstract String getUserAgent();

	protected abstract SD getSessionData();

	protected abstract Response intercept(Chain chain) throws IOException;

}
