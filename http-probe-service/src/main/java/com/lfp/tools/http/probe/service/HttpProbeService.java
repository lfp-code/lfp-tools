package com.lfp.tools.http.probe.service;

import java.util.function.Function;

import com.lfp.joe.net.proxy.Proxy;

public interface HttpProbeService extends Function<Proxy, HttpProbeContext> {

}
