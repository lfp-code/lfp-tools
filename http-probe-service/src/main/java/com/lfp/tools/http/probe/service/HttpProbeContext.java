package com.lfp.tools.http.probe.service;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import com.lfp.browser.webdriver.client.driver.WebDriverLFP;
import com.lfp.browser.webdriver.client.driver.chrome.ChromeOptionsLFP;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.crypto.Hashable;
import com.lfp.joe.utils.function.RateLimiter;
import com.lfp.joe.core.function.Throws.ThrowingConsumer;
import com.lfp.joe.core.function.Throws.ThrowingFunction;

import at.favre.lib.bytes.Bytes;
import okhttp3.OkHttpClient;

public interface HttpProbeContext extends Scrapable, Hashable {

	@Override
	default Bytes hash() {
		return Utils.Crypto.hashMD5(uuid());
	}

	String uuid();

	Optional<String> getProxyPublicIPAddress();

	Optional<Proxy> getProxy();

	OkHttpClient getClient();

	RateLimiter getRateLimiter();

	Map<String, Object> getAttributes();

	<X> X webDriverAccess(Consumer<ChromeOptionsLFP> optionsSetup,
			ThrowingFunction<WebDriverLFP, X, IOException> accessor) throws IOException;

	default void webDriverAccess(Consumer<ChromeOptionsLFP> optionsSetup,
			ThrowingConsumer<WebDriverLFP, IOException> accessor) throws IOException {
		this.webDriverAccess(optionsSetup, accessor == null ? null : wd -> {
			accessor.accept(wd);
			return Nada.get();
		});
	}

}
