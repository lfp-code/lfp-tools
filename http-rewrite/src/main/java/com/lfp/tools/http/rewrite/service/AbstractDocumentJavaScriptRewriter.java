package com.lfp.tools.http.rewrite.service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Optional;

import org.immutables.value.Value;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.http.rewrite.service.ImmutableJavascriptRewriter.Script;

import at.favre.lib.bytes.Bytes;

@ValueLFP.Style
@Value.Immutable
@Value.Enclosing
public abstract class AbstractDocumentJavaScriptRewriter
		extends AbstractJavascriptRewriter<Document, Element, RuntimeException> {

	private static final boolean HEADER_DEFAULT = true;

	@Override
	public Bytes get(Document input) throws RuntimeException {
		return Utils.Bits.from(input.outerHtml().getBytes(input.charset()));
	}

	@Override
	protected Element parent(Document input, Script script) {
		if (isHeader(script))
			return selectFirstOrCreate(input, "head", true);
		else
			return selectFirstOrCreate(input, "footer", true);
	}

	@Override
	protected Document rewrite(Document input, Element parent, Script script) {
		Element scriptElement;
		if (script.prepend())
			scriptElement = parent.prependElement("script");
		else
			scriptElement = parent.appendElement("script");
		String data = String.format("\n%s\n", script.value());
		scriptElement
				.attr("type", "text/javascript")
				.appendChild(new DataNode(data));
		return input;
	}

	private static Element selectFirstOrCreate(Document input, String tagName, boolean prepend) {
		return Optional.ofNullable(input.selectFirst(tagName)).orElseGet(() -> {
			if (prepend)
				return input.prependElement(tagName);
			return input.appendElement(tagName);
		});
	}

	private static boolean isHeader(Script script) {
		return CoreReflections.tryCast(script, AbstractDocumentScript.class)
				.map(AbstractDocumentScript::header)
				.orElse(HEADER_DEFAULT);
	}

	@Value.Immutable
	static abstract class AbstractDocumentScript extends AbstractScript {

		@Value.Default
		public boolean header() {
			return HEADER_DEFAULT;
		}

	}

	public static void main(String[] args) throws IOException, InterruptedException {
		var uri = URI.create("https://example.com/");
		var response = HttpClients.get().send(HttpRequests.request().uri(uri), BodyHandlers.ofString());
		System.out.println(response.body());
		var doc = Jsoups.tryParse(response.body(), response.headers().map(), uri).get();
		System.out.println(doc);
		var body = DocumentJavaScriptRewriter.builder().input(doc).addScripts(Script.of("alert('yo');")).build().get();
		System.out.println(body.encodeUtf8());
	}
}
