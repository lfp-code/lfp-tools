package com.lfp.tools.http.rewrite.cache;

import java.io.Serializable;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import org.apache.commons.lang3.Validate;
import org.immutables.value.Value;
import org.jsoup.Connection.Method;

import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import io.mikael.urlbuilder.UrlBuilder;
import io.undertow.server.HttpServerExchange;

@Value.Immutable
public abstract class AbstractRequest implements Function<URI, HttpRequest>, Serializable {

	private static final long serialVersionUID = 6639923002958333498L;
	private static final List<Method> SUPPORTED_METHODS = Streams.of(Method.values())
			.filter(v -> !v.hasBody())
			.toImmutableList();

	public static ImmutableRequest of(HttpServerExchange exchange) {
		var uri = UndertowUtils.getRequestURI(exchange);
		var method = Streams.of(Method.values())
				.filter(v -> v.name().equalsIgnoreCase(exchange.getRequestMethod().toString()))
				.findFirst()
				.orElse(Method.GET);
		return ImmutableRequest.builder()
				.method(method)
				.uri(uri)
				.headers(UndertowUtils.headerMap(exchange.getRequestHeaders()))
				.build();
	}

	public abstract Method method();

	public abstract URI uri();

	public abstract HeaderMap headers();

	@Value.Lazy
	public String uuid() {
		var md = Utils.Crypto.getMessageDigestMD5();
		Utils.Crypto.update(md, method());
		Utils.Crypto.update(md, uri());
		int index = -1;
		for (var ent : headers().stream(Headers.ACCEPT, Headers.ACCEPT_ENCODING, Headers.USER_AGENT)) {
			index++;
			Utils.Crypto.update(md, index);
			Utils.Crypto.update(md, ent.getKey());
			Utils.Crypto.update(md, ent.getValue());
		}
		return Utils.Bits.from(md.digest()).encodeHex();
	}

	@Override
	public HttpRequest apply(URI upstreamURI) {
		var requestHeaders = headers()
				.withRemoved(Headers.CONNECTION)
				.withRemoved(Headers.HOST);
		var requestURI = UrlBuilder.fromUri(upstreamURI)
				.withPath(uri().getPath())
				.withQuery(uri().getQuery())
				.toUri();
		var upstreamRequest = HttpRequests.request()
				.method(method().name(), BodyPublishers.noBody())
				.uri(requestURI)
				.headers(requestHeaders.httpHeaders())
				.build();
		return upstreamRequest;
	}

	@Value.Check
	protected void validate() {
		Validate.isTrue(SUPPORTED_METHODS.contains(method()), "unsupported method - %s", this);
	}

	@Value.Check
	protected AbstractRequest normalize() {
		var uri = uri();
		var uriNormalized = URIs.normalize(uri);
		if (uri.toString().equals(uriNormalized.toString()))
			return this;
		return ImmutableRequest.copyOf(this).withUri(uriNormalized);
	}

	@Override
	public int hashCode() {
		return Objects.hash(uuid());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractRequest other = (AbstractRequest) obj;
		return Objects.equals(uuid(), other.uuid());
	}

}
