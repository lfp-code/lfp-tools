package com.lfp.tools.http.rewrite.cache;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Objects;

import org.immutables.value.Value;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.status.StatusCodes;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;

@Value.Immutable
public abstract class AbstractResponse implements HttpHandler, Serializable {

	public static final ImmutableResponse NOT_FOUND;
	static {
		var code = StatusCodes.NOT_FOUND;
		var body = StatusCodes.getReason(code).getBytes(MachineConfig.getDefaultCharset());
		var headers = HeaderMap.of()
				.with(Headers.CONTENT_LENGTH, Objects.toString(body.length))
				.with(Headers.CONTENT_TYPE, String.format("text/plain; charset=%s", MachineConfig.getDefaultCharset()));
		NOT_FOUND = ImmutableResponse.builder().code(code).headers(headers).body(body).build();
	}

	private static final long serialVersionUID = 946424162278948728L;

	public abstract int code();

	public abstract HeaderMap headers();

	public abstract byte[] body();

	@Override
	public void handleRequest(HttpServerExchange exchange) {
		exchange.setStatusCode(code());
		for (var ent : headers().map().entrySet()) {
			var name = ent.getKey();
			var values = ent.getValue();
			exchange.getResponseHeaders().addAll(HttpString.tryFromString(name), values);
		}
		exchange.getResponseSender().send(ByteBuffer.wrap(body()));
	}

}
