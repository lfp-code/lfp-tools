package com.lfp.tools.http.rewrite.service;

import java.util.List;

import org.apache.commons.lang3.Validate;
import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;
import com.lfp.tools.http.rewrite.service.ImmutableJavascriptRewriter.Script;

import at.favre.lib.bytes.Bytes;;

@ValueLFP.Style
@Value.Enclosing
public abstract class AbstractJavascriptRewriter<X, Y, E extends Exception> implements Rewriter<X, E> {

	public abstract List<Script> scripts();

	@Override
	public Bytes get() throws E {
		X input = input();
		for (var script : scripts()) {
			Y parent = parent(input, script);
			if (parent == null)
				continue;
			input = rewrite(input, parent, script);
		}
		return get(input);
	}

	@Value.NonAttribute
	protected abstract Y parent(X input, Script script);

	@Value.NonAttribute
	protected abstract X rewrite(X input, Y parent, Script script);

	protected abstract Bytes get(X input) throws E;

	@Value.Immutable
	static abstract class AbstractScript {

		@Value.Parameter
		public abstract String value();

		@Value.Default
		public boolean prepend() {
			return false;
		}

		@Value.Check
		protected void validate() {
			Validate.notBlank(value());
		}

	}

}
