package com.lfp.tools.http.rewrite.service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.Charset;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.http.rewrite.service.ImmutableJavascriptRewriter.Script;

import at.favre.lib.bytes.Bytes;

@ValueLFP.Style
@Value.Immutable
public abstract class AbstractStringJavaScriptRewriter
		extends AbstractJavascriptRewriter<String, String, RuntimeException> {

	@Value.Parameter
	public abstract Charset charset();

	@Override
	protected Bytes get(String input) throws RuntimeException {
		return Utils.Bits.from(input.getBytes(charset()));
	}

	@Override
	protected String parent(String input, Script script) {
		return input;
	}

	@Override
	protected String rewrite(String input, String parent, Script script) {
		String value = script.value();
		if (Utils.Strings.isBlank(input))
			return value;
		return input += "\n" + value;
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		var uri = URI.create("https://browser.sentry-cdn.com/7.17.3/bundle.es5.min.js");
		var response = HttpClients.get().send(HttpRequests.request().uri(uri), BodyHandlers.ofString());
		System.out.println(response.body());
		var body = StringJavaScriptRewriter.builder()
				.input(response.body())
				.addScripts(Script.of("alert('yo');"))
				.build()
				.get();
		System.out.println(body.encodeUtf8());
	}
}
