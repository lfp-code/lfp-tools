package com.lfp.tools.http.rewrite;

import java.net.http.HttpResponse.BodyHandlers;
import java.util.function.Function;

import org.immutables.value.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.lfp.connect.undertow.Undertows;
import com.lfp.connect.undertow.handler.ErrorLoggingHandler;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.http.rewrite.cache.HttpRewriteCache;
import com.lfp.tools.http.rewrite.cache.ImmutableRequest;
import com.lfp.tools.http.rewrite.cache.ImmutableResponse;
import com.lfp.tools.http.rewrite.config.HttpRewriteConfig;
import com.lfp.tools.http.rewrite.service.ImmutableJavascriptRewriter.Script;

import io.undertow.Undertow.ListenerInfo;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

@ValueLFP.Style
@Value.Enclosing
public class App implements Runnable, HttpHandler {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final Logger LOGGER = LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		new App(args).run();
	}

	private final Function<ImmutableRequest, ImmutableResponse> cache;

	protected App(String[] args) {
		var cfg = Configs.get(HttpRewriteConfig.class);
		var propertyName = Streams.of(BeanRef.$(HttpRewriteConfig::javaScript))
				.map(BeanPath::getPath)
				.flatArray(Utils.Strings::splitByCharacterTypeCamelCase)
				.map(Utils.Strings::upperCase)
				.joining("_");
		var scripts = Streams.of(cfg.javaScript(),
				System.getenv(propertyName),
				System.getProperty(propertyName))
				.prepend(Streams.of(args))
				.mapPartial(Utils.Strings::trimToNullOptional)
				.distinct()
				.map(Throws.functionUnchecked(value -> {
					var uri = URIs.parse(value).orElse(null);
					if (uri == null)
						return value;
					var response = HttpClients.get().send(HttpRequests.request().uri(uri), BodyHandlers.ofString());
					StatusCodes.validate(response.statusCode());
					return response.body();
				}))
				.map(Script::of);
		this.cache = new HttpRewriteCache(scripts);
	}

	@Override
	public void run() {
		LOGGER.info("starting server. upstreamURI:{}",
				Configs.get(HttpRewriteConfig.class).upstreamURI());
		var server = Undertows.serverBuilder().setHandler(new ErrorLoggingHandler(this)).build();
		try {
			server.start();
			LOGGER.info("server started:{}",
					Streams.of(server.getListenerInfo()).map(ListenerInfo::getAddress).toList());
			Thread.currentThread().join();
		} catch (InterruptedException e) {
			throw Throws.unchecked(e);
		} finally {
			server.stop();
		}
	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		var response = cache.apply(ImmutableRequest.of(exchange));
		response.handleRequest(exchange);
	}

}
