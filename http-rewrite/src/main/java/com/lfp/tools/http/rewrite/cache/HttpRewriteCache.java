package com.lfp.tools.http.rewrite.cache;

import java.net.URI;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import org.ehcache.config.units.EntryUnit;
import org.ehcache.config.units.MemoryUnit;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.net.MediaType;
import com.google.common.reflect.TypeToken;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.ehcache.EhcacheLFP;
import com.lfp.joe.cache.ehcache.EhcacheOptions;
import com.lfp.joe.cache.ehcache.ExpiryPolicyLFP;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.EmptyArrays;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.http.rewrite.config.HttpRewriteConfig;
import com.lfp.tools.http.rewrite.service.DocumentJavaScriptRewriter;
import com.lfp.tools.http.rewrite.service.ImmutableJavascriptRewriter.Script;
import com.lfp.tools.http.rewrite.service.Rewriter;
import com.lfp.tools.http.rewrite.service.StringJavaScriptRewriter;

import at.favre.lib.bytes.Bytes;

public class HttpRewriteCache implements Function<ImmutableRequest, ImmutableResponse> {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final Logger LOGGER = LoggerFactory.getLogger(THIS_CLASS);
	private static final boolean INFO_LOG = MachineConfig.isDeveloper() || MachineConfig.isJavaDebugWireProtocol()
			|| Configs.get(HttpRewriteConfig.class).verboseLogging();

	private final EhcacheLFP<String, ImmutableResponse> cache;
	private final List<Script> scripts;

	public HttpRewriteCache(Iterable<? extends Script> scripts) {
		var cfg = Configs.get(HttpRewriteConfig.class);
		var expiryPolicy = ExpiryPolicyLFP.<String, ImmutableResponse>empty()
				.append(r -> r.elapsed().map(cfg.cacheExpireAfterWrite()::minus).orElse(null))
				.append(ExpiryPolicyLFP.of(cfg.cacheExpireAfterAccess()));
		var cacheOps = EhcacheOptions.of(TypeToken.of(String.class), TypeToken.of(ImmutableResponse.class), THIS_CLASS)
				.withExpiryPolicy(expiryPolicy)
				.withResourcePoolsBuilderConfiguration(rpb -> {
					return rpb.heap(cfg.cacheMaximumCountHeap(), EntryUnit.ENTRIES)
							.disk(cfg.cacheMaximumSizeDisk(), MemoryUnit.B);
				});
		this.cache = Caches.newEhcache(cacheOps);
		this.scripts = Streams.of(scripts).nonNull().distinct().<Script>map(v -> v).toImmutableList();
		if (INFO_LOG)
			for (int i = 0; i < this.scripts.size(); i++)
				LOGGER.info("script {}:\n{}\n", i + 1, this.scripts.get(i));
	}

	@Override
	public ImmutableResponse apply(ImmutableRequest request) {
		return cache.get(request.uuid(), Throws.functionUnchecked(nil -> load(request)));
	}

	protected ImmutableResponse load(ImmutableRequest request) throws Exception {
		log("load requested. uuid:{} request:{}", request.uuid(), request);
		var cfg = Configs.get(HttpRewriteConfig.class);
		var upstreamRequest = request.apply(cfg.upstreamURI());
		var response = HttpClients.get().send(upstreamRequest, BodyHandlers.ofInputStream());
		Bytes body;
		try (var bodyInputStream = response.body();) {
			body = Utils.Bits.from(bodyInputStream);
		}
		var result = load(response, body);
		log("load complete.  uuid:{} request:{} result:{}", request.uuid(), request, result);
		return result;
	}

	protected ImmutableResponse load(HttpResponse<?> response, Bytes body) throws Exception {
		var requestURI = response.request().uri();
		var responseHeaders = HeaderMap.of(response.headers());
		var responseCharset = Headers.tryGetCharset(responseHeaders).orElseGet(MachineConfig::getDefaultCharset);
		var rewriter = tryGetDocument(requestURI, responseHeaders, responseCharset, body).<Rewriter<?, ?>>map(input -> {
			return DocumentJavaScriptRewriter.of(input).withScripts(this.scripts);
		}).or(() -> tryGetString(requestURI, responseHeaders, responseCharset, body).map(input -> {
			return StringJavaScriptRewriter.of(input, responseCharset).withScripts(this.scripts);
		})).orElse(null);
		if (rewriter == null)
			return ImmutableResponse.NOT_FOUND;
		var updatedBody = rewriter.get();
		var updatedHeaders = responseHeaders.withReplace(Headers.CONTENT_LENGTH,
				Objects.toString(updatedBody.length()));
		return ImmutableResponse.builder()
				.code(response.statusCode())
				.headers(updatedHeaders)
				.body(updatedBody.array())
				.build();
	}

	protected static Optional<Document> tryGetDocument(URI requestURI, HeaderMap responseHeaders, Charset charset,
			Bytes body) {
		boolean valid = isExtension(requestURI, "htm", "html");
		if (!valid)
			valid = isSubtype(responseHeaders, "html");
		if (!valid)
			return Optional.empty();
		ThrowingSupplier<Document, ?> docSupplier = () -> {
			return Jsoups.parse(body.inputStream(), charset, requestURI);
		};
		var doc = docSupplier.onError(t -> {
			log("document parse failed. requestURI:{} responseHeaders:{}", requestURI, responseHeaders);
			return null;
		}).get();
		return Optional.ofNullable(doc);
	}

	protected static Optional<String> tryGetString(URI requestURI, HeaderMap responseHeaders, Charset charset,
			Bytes body) {
		boolean valid = isExtension(requestURI, "js");
		if (!valid)
			valid = isSubtype(responseHeaders, "javascript");
		if (!valid)
			return Optional.empty();
		return Optional.of(body.encodeCharset(charset));
	}

	protected static boolean isExtension(URI uri, String... filters) {
		var path = uri.getPath();
		var ext = Utils.Strings.substringAfterLast(path, ".");
		if (Utils.Strings.isBlank(ext))
			return false;
		return Streams.of(filters).anyMatch(v -> Utils.Strings.equalsIgnoreCase(ext, v));
	}

	protected static boolean isSubtype(HeaderMap headers, String... filters) {
		return Headers.tryGetMediaType(headers)
				.map(MediaType::subtype)
				.map(subtype -> {
					return Streams.of(filters).anyMatch(v -> Utils.Strings.containsIgnoreCase(subtype, v));
				})
				.orElse(false);
	}

	protected static void log(String message, Object... arguments) {
		arguments = Streams.of(arguments).map(argument -> {
			if (argument instanceof ImmutableResponse)
				return ((ImmutableResponse) argument).withBody(EmptyArrays.ofByte());
			return argument;
		}).toArray();
		if (INFO_LOG)
			LOGGER.info(message, arguments);
		else
			LOGGER.debug(message, arguments);

	}

}
