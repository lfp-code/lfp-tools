package com.lfp.tools.http.rewrite.service;

import org.immutables.value.Value;

import com.lfp.joe.core.function.Throws.ThrowingSupplier;

import at.favre.lib.bytes.Bytes;

public interface Rewriter<X, E extends Exception> extends ThrowingSupplier<Bytes, E> {

	@Value.Parameter
	X input();

}
