package com.lfp.tools.http.rewrite.config;

import java.net.URI;
import java.time.Duration;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.converter.URIConverter;
import com.lfp.joe.properties.converter.DurationConverter;

public interface HttpRewriteConfig extends Config {

	@DefaultValue("https://localhost:80")
	@ConverterClass(URIConverter.class)
	URI upstreamURI();

	String javaScript();

	@DefaultValue("1 min")
	@ConverterClass(DurationConverter.class)
	Duration cacheExpireAfterWrite();

	@DefaultValue("10 sec")
	@ConverterClass(DurationConverter.class)
	Duration cacheExpireAfterAccess();

	@DefaultValue("100000")
	long cacheMaximumCountHeap();

	@DefaultValue("1073741824") // 10 gb
	long cacheMaximumSizeDisk();

	@DefaultValue("false") // 10 gb
	boolean verboseLogging();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
	}
}
