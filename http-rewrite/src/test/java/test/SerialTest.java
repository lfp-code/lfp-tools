package test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import com.lfp.joe.utils.Utils;

public class SerialTest {

	public static void main(String[] args) throws IOException {
		var bytes = Utils.Bits.from(Utils.Crypto.getRandomString(100_000));
		try (var baos = new ByteArrayOutputStream(); var oos = new ObjectOutputStream(baos)) {
			oos.writeObject(bytes);
			System.out.println(baos.toByteArray().length);
		}

	}

}
