package test;

import java.net.URI;

import com.lfp.joe.core.properties.Configs;
import com.lfp.tools.http.rewrite.config.HttpRewriteConfig;

import io.mikael.urlbuilder.UrlBuilder;

public class SlashTest {

	public static void main(String[] args) {
		var uri = URI.create("http://127.0.0.1:8080");
		var upstreamURI = Configs.get(HttpRewriteConfig.class).upstreamURI();
		var requestURI = UrlBuilder.fromUri(upstreamURI)
				.withPath(uri.getPath())
				.withQuery(uri.getQuery())
				.toUri();
		System.out.println(requestURI);
	}
}
