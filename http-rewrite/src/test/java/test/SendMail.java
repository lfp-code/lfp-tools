package test;

import java.util.Properties;

import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;

public class SendMail {

	public static void main(String[] args) throws AddressException, MessagingException {
		Properties prop = new Properties();
		prop.put("mail.smtp.host", "localhost");
		prop.put("mail.smtp.port", "25");
		Session session = Session.getInstance(prop);
		var message = new MimeMessage(session);
		message.setFrom(new InternetAddress("admin@lfpconnect.io"));
		message.setRecipients(
				Message.RecipientType.TO,
				InternetAddress.parse("reggie.pierce@gmail.com"));
		message.setSubject("Mail Subject");

		String msg = "This is another test";

		MimeBodyPart mimeBodyPart = new MimeBodyPart();
		mimeBodyPart.setContent(msg, "text/html; charset=utf-8");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(mimeBodyPart);

		message.setContent(multipart);

		Transport.send(message);
		System.err.println("done");
	}

}
