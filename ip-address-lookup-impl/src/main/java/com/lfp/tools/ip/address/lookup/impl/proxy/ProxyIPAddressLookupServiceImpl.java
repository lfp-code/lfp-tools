package com.lfp.tools.ip.address.lookup.impl.proxy;

import java.io.IOException;
import java.time.Duration;
import java.util.Optional;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.caffeine.writerloader.CacheWriterLoader;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.tools.ip.address.lookup.service.proxy.ProxyIPAddressLookupService;

public class ProxyIPAddressLookupServiceImpl implements ProxyIPAddressLookupService {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static final ProxyIPAddressLookupServiceImpl DEFAULT_INSTANCE = new Redis(TendisClients.getDefault(),
			Duration.ofHours(1), 20_000, null, Duration.ofSeconds(15), THIS_CLASS, "default");

	private final LoadingCache<Proxy, Optional<String>> ipCache;

	public ProxyIPAddressLookupServiceImpl(CacheWriterLoader<Proxy, Optional<String>> ipWriterLoader,
			long memoryMaximumSize, Duration memoryExpireAfterWrite, Duration memoryExpireAfterAccess) {
		this.ipCache = ipWriterLoader.buildCache(
				Caches.newCaffeineBuilder(memoryMaximumSize, memoryExpireAfterWrite, memoryExpireAfterAccess));
	}

	@Override
	public String getPublicIP(Proxy proxy) throws IOException {
		if (proxy == null)
			return null;
		return this.ipCache.get(proxy).orElse(null);
	}

	public static class Redis extends ProxyIPAddressLookupServiceImpl {

		public Redis(RedissonClientLFP redissonClient, Duration proxyTTL, long memoryMaximumSize,
				Duration memoryExpireAfterWrite, Duration memoryExpireAfterAccess, Object keyPrefixPart,
				Object... keyPrefixParts) {
			super(new ProxyRedisWriterLoader(redissonClient, proxyTTL, keyPrefixPart, keyPrefixParts),
					memoryMaximumSize, memoryExpireAfterWrite, memoryExpireAfterAccess);
		}
	}

}
