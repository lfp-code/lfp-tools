package com.lfp.tools.ip.address.lookup.impl.info;

import java.time.Duration;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.tools.cache.RedissonWriterLoader;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.net.http.ip.IPAddressInfo;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public class InfoRedisWriterLoader extends RedissonWriterLoader<String, StatValue<IPAddressInfo>> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private Duration timeToLive;

	public InfoRedisWriterLoader(RedissonClientLFP client, Duration timeToLive, Object keyPrefixPart,
			Object... keyPrefixParts) {
		super(client, keyPrefixPart, keyPrefixParts);
		this.timeToLive = timeToLive;
	}

	@Override
	protected @Nullable StatValue<IPAddressInfo> loadFresh(@NonNull String key) throws Exception {
		var resultOp = IPs.getIPAddressInfo(key);
		return StatValue.build(resultOp.orElse(null));
	}

	@Override
	protected @Nullable Iterable<Object> getKeyParts(@NonNull String key) {
		return List.of(key);
	}

	@Override
	protected @Nullable ThrowingFunction<ThrowingSupplier<StatValue<IPAddressInfo>, Exception>, StatValue<IPAddressInfo>, Exception> getStorageLockAccessor(
			@NonNull String key, String lockKey) {
		return createStorageLockAccessorDefault(key, lockKey);
	}

	@Override
	protected @Nullable Duration getStorageTTL(@NonNull String key, @NonNull StatValue<IPAddressInfo> result) {
		return timeToLive;
	}

	public static void main(String[] args) {
		var cache = new InfoRedisWriterLoader(TendisClients.getDefault(), Duration.ofDays(7), THIS_CLASS, "v2")
				.buildCache(Caches.newCaffeineBuilder(10_000, null, Duration.ofSeconds(10)));
		System.out.println(cache.get("http://nexus.lfpcode.com"));
		System.out.println(cache.get("http://" + Utils.Crypto.getSecureRandomString() + ".com"));
	}

}
