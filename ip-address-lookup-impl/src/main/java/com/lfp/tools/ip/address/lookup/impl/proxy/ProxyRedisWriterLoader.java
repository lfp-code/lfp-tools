package com.lfp.tools.ip.address.lookup.impl.proxy;

import java.time.Duration;
import java.util.List;
import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.tools.cache.RedissonWriterLoader;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public class ProxyRedisWriterLoader extends RedissonWriterLoader<Proxy, Optional<String>> {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private Duration timeToLive;

	public ProxyRedisWriterLoader(RedissonClientLFP client, Duration timeToLive, Object keyPrefixPart,
			Object... keyPrefixParts) {
		super(client, keyPrefixPart, keyPrefixParts);
		this.timeToLive = timeToLive;
	}

	@Override
	protected @Nullable Optional<String> loadFresh(@NonNull Proxy key) throws Exception {
		var ipAddress = Ok.Clients.getIPAddress(Ok.Clients.get(key));
		if (!IPs.isValidIpAddress(ipAddress))
			return Optional.empty();
		return Optional.of(ipAddress);
	}

	@Override
	protected @Nullable Iterable<Object> getKeyParts(@NonNull Proxy key) {
		return List.of(key.uuid());
	}

	@Override
	protected @Nullable ThrowingFunction<ThrowingSupplier<Optional<String>, Exception>, Optional<String>, Exception> getStorageLockAccessor(
			@NonNull Proxy key, String lockKey) {
		return createStorageLockAccessorDefault(key, lockKey);
	}

	@Override
	protected @Nullable Duration getStorageTTL(@NonNull Proxy key, @NonNull Optional<String> result) {
		return timeToLive;
	}

}
