package com.lfp.tools.ip.address.lookup.impl.info;

import java.time.Duration;
import java.util.List;
import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.tools.cache.RedissonWriterLoader;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingFunction;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;

public class IPRedisWriterLoader extends RedissonWriterLoader<String, Optional<String>> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 1;
	private Duration timeToLive;

	public IPRedisWriterLoader(RedissonClientLFP client, Duration timeToLive, Object keyPrefixPart,
			Object... keyPrefixParts) {
		super(client, keyPrefixPart, keyPrefixParts);
		this.timeToLive = timeToLive;
	}

	@Override
	protected @Nullable Optional<String> loadFresh(@NonNull String key) throws Exception {
		return IPs.parse(key);
	}

	@Override
	protected @Nullable Iterable<Object> getKeyParts(@NonNull String key) {
		return List.of(key);
	}

	@Override
	protected @Nullable ThrowingFunction<ThrowingSupplier<Optional<String>, Exception>, Optional<String>, Exception> getStorageLockAccessor(
			@NonNull String key, String lockKey) {
		return null;
	}

	@Override
	protected @Nullable Duration getStorageTTL(@NonNull String key, @NonNull Optional<String> result) {
		return this.timeToLive;
	}

	public static void main(String[] args) {
		var cache = new IPRedisWriterLoader(TendisClients.getDefault(), Duration.ofDays(7), THIS_CLASS, "v1")
				.buildCache(Caches.newCaffeineBuilder(10_000, null, Duration.ofSeconds(10)));
		System.out.println(cache.get("http://nexus.lfpcode.com"));
		System.out.println(cache.get("http://" + Utils.Crypto.getSecureRandomString() + ".com"));
		System.exit(0);
	}
}
