package com.lfp.tools.ip.address.lookup.impl.info;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Optional;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.cache.caffeine.writerloader.CacheWriterLoader;
import com.lfp.joe.net.http.ip.IPAddressInfo;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.ip.address.lookup.service.info.IPAddressLookupService;

public class IPAddressLookupServiceImpl implements IPAddressLookupService {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static IPAddressLookupServiceImpl DEFAULT_INSTANCE = new Redis(TendisClients.getDefault(),
			Duration.ofMinutes(5), TendisClients.getDefault(), Duration.ofDays(2), 20_000, null, Duration.ofSeconds(15),
			THIS_CLASS, 0);

	private final LoadingCache<String, Optional<String>> ipCache;
	private final LoadingCache<String, StatValue<IPAddressInfo>> infoCache;

	public IPAddressLookupServiceImpl(CacheWriterLoader<String, Optional<String>> ipWriterLoader,
			CacheWriterLoader<String, StatValue<IPAddressInfo>> infoWriterLoader, long memoryMaximumSize,
			Duration memoryExpireAfterWrite, Duration memoryExpireAfterAccess) {
		this.ipCache = ipWriterLoader.buildCache(
				Caches.newCaffeineBuilder(memoryMaximumSize, memoryExpireAfterWrite, memoryExpireAfterAccess));
		this.infoCache = infoWriterLoader.buildCache(
				Caches.newCaffeineBuilder(memoryMaximumSize, memoryExpireAfterWrite, memoryExpireAfterAccess));
	}

	@Override
	public String getIp(String lookup) throws IOException {
		return ipCache.get(lookup).orElse(null);
	}

	@Override
	public IPAddressInfo getInfo(String lookup) throws IOException {
		return infoCache.get(lookup).getValue();
	}

	public static class Redis extends IPAddressLookupServiceImpl {

		public Redis(RedissonClientLFP ipClient, Duration ipTTL, RedissonClientLFP infoClient, Duration infoTTL,
				long memoryMaximumSize, Duration memoryExpireAfterWrite, Duration memoryExpireAfterAccess,
				Object keyPrefixPart, Object... keyPrefixParts) {
			super(new IPRedisWriterLoader(ipClient, ipTTL, keyPrefixPart,
					Utils.Lots.stream(keyPrefixParts).append("ip").toArray(Object.class)),
					new InfoRedisWriterLoader(infoClient, infoTTL, keyPrefixPart,
							Utils.Lots.stream(keyPrefixParts).append("info").toArray(Object.class)),
					memoryMaximumSize, memoryExpireAfterWrite, memoryExpireAfterAccess);
		}
	}

	public static void main(String[] args) throws IOException {
		var rand = String.format("http://%s.com", Utils.Crypto.getSecureRandomString());
		for (int i = 0; i < 10; i++) {
			for (var test : List.of("https://nexus.lfpcode.com", rand)) {
				var info = IPAddressLookupServiceImpl.DEFAULT_INSTANCE.getInfo(test);
				System.out.println(Serials.Gsons.getPretty().toJson(info));
			}
		}
	}
}
