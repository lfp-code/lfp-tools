package test;

import com.lfp.joe.net.proxy.Proxy;

import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.client.ProxyRoutingClient;
import com.lfp.tools.ip.address.lookup.impl.proxy.ProxyIPAddressLookupServiceImpl;

public class ProxyTest {
	public static void main(String[] args) throws Exception {
		String proxyJson = "{\n"
				+ "  \"publicIPAddress\": \"158.255.208.155\",\n"
				+ "  \"type\": \"HTTP\",\n"
				+ "  \"hostname\": \"158.255.208.155\",\n"
				+ "  \"port\": 60099,\n"
				+ "  \"username\": \"reggiepierce\",\n"
				+ "  \"password\": \"NreXzJSczVnPdumhNgPirHeru3te\"\n"
				+ "}";
		Proxy proxy;
		if (Utils.Strings.isNotBlank(proxyJson))
			proxy = Serials.Gsons.get().fromJson(proxyJson, Proxy.class);
		else {
			proxy = ProxyRoutingClient.get().lookupProxyBlocking();
			System.out.println(Serials.Gsons.get().toJson(proxy));
		}
		System.out.println(ProxyIPAddressLookupServiceImpl.DEFAULT_INSTANCE.getPublicIP(proxy));
		System.exit(0);
	}
}
