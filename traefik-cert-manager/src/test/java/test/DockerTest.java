package test;

import java.time.Duration;
import java.util.Arrays;
import java.util.Map;

import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientImpl;
import com.github.dockerjava.zerodep.ZerodepDockerHttpClient;
import com.lfp.joe.stream.Streams;

public class DockerTest {

	public static void main(String[] args) {
		var config = DefaultDockerClientConfig.createDefaultConfigBuilder()
				.withDockerHost("tcp://localhost:7777")
				.withDockerTlsVerify(false)
				.build();
		var httpClient = new ZerodepDockerHttpClient.Builder()
				.dockerHost(config.getDockerHost())
				.sslConfig(config.getSSLConfig())
				.maxConnections(100)
				.connectionTimeout(Duration.ofSeconds(30))
				.responseTimeout(Duration.ofSeconds(45))
				.build();
		var dockerClient = DockerClientImpl.getInstance(config, httpClient);
		var resp = dockerClient.createContainerCmd("ubuntu:latest")
				.withCmd("bash", "-c", "echo 'dude'; sleep infinity")
				.withLabels(Map.of("neat", "wow"))
				.exec();
		dockerClient.startContainerCmd(resp.getId()).exec();
		System.out.println(resp);
		Streams.of(dockerClient.listContainersCmd().exec()).forEach(container -> {
			System.out.println(Arrays.asList(container.getNames()));
			System.out.println(container.getNetworkSettings().getNetworks());
		});

	}

}
