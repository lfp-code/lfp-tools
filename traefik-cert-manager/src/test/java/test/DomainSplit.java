package test;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.traefik.cert.manager.config.TraefikCertManagerConfig;

public class DomainSplit {

	public static void main(String[] args) {
		var cfg = Configs.get(TraefikCertManagerConfig.class);
		var domains = Streams.of(cfg.domains())
				.mapPartial(Utils.Strings::trimToNullOptional)
				.flatMap(domain -> {
					if (!cfg.generateWildcards())
						return Streams.of(domain);
					return Streams.of(domain.split("^\\*?\\."))
							.mapPartial(Utils.Strings::trimToNullOptional)
							.limit(1)
							.map(v -> String.format("*.%s", v))
							.prepend(domain);
				})
				.distinct()
				.toImmutableList();
		System.out.println(domains);
	}
}
