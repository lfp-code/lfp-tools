package com.lfp.tools.traefik.cert.manager.store;

import java.util.Date;
import java.util.List;

import org.immutables.gson.Gson;
import org.immutables.value.Value;

import com.google.gson.annotations.SerializedName;
import com.google.re2j.Pattern;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.tools.traefik.cert.manager.store.CertificateContext.IssuerData;
import com.lfp.tools.traefik.cert.manager.store.CertificateContext.Metadata;

import at.favre.lib.bytes.Bytes;

@Gson.TypeAdapters
@Value.Immutable
@Value.Enclosing
@ValueLFP.Style
abstract class AbstractCertificateContext {

	abstract String path();

	abstract Date modified();

	abstract Metadata metadata();

	abstract Bytes crt();

	abstract Bytes key();

	@Value.Lazy
	public String name() {
		var parts = path().split(Pattern.quote("/"));
		return parts[parts.length - 1];
	}

	@Value.Immutable
	static abstract class AbstractMetadata {

		public abstract List<String> sans();

		@SerializedName("issuer_data")
		public abstract List<IssuerData> issuerData();

	}

	@Value.Immutable
	static abstract class AbstractIssuerData {

		public abstract String url();

	}
}
