package com.lfp.tools.traefik.cert.manager.store;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;

import org.immutables.gson.Gson;
import org.immutables.value.Value;

import com.google.gson.annotations.SerializedName;
import com.lfp.joe.core.config.ValueLFP;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.traefik.cert.manager.store.TraefikConfiguration.Certificate;
import com.lfp.tools.traefik.cert.manager.store.TraefikConfiguration.TLS;

@Gson.TypeAdapters
@ValueLFP.Style
@Value.Immutable
@Value.Enclosing
abstract class AbstractTraefikConfiguration {

	public abstract TLS tls();

	@Value.Immutable
	static abstract class AbstractTLS {

		@Value.Parameter
		public abstract List<Certificate> certificates();

	}

	@Value.Immutable
	static abstract class AbstractCertificate {

		public static Certificate of(File keyFile, File certFile) {
			Objects.requireNonNull(keyFile, "keyFile required");
			Objects.requireNonNull(certFile, "certFile required");
			BiConsumer<File, List<String>> validateExtension = (file, exts) -> {
				var fileExt = Utils.Strings.substringAfterLast(file.getName(), ".");
				if (Utils.Strings.isBlank(fileExt)
						|| exts.stream().anyMatch(ext -> Utils.Strings.equalsIgnoreCase(fileExt, ext)))
					return;
				throw new IllegalArgumentException(String.format("invalid file:%s exts:%s", file, exts));
			};
			validateExtension.accept(keyFile, List.of("key"));
			validateExtension.accept(certFile, List.of("crt", "cert"));
			return Certificate.builder()
					.keyFilePath(keyFile.getAbsolutePath())
					.certFilePath(certFile.getAbsolutePath())
					.build();
		}

		@SerializedName("keyFile")
		public abstract String keyFilePath();

		@SerializedName("certFile")
		public abstract String certFilePath();

		@Value.Check
		protected void validate() {

		}

	}
}
