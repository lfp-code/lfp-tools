package com.lfp.tools.traefik.cert.manager.store;

import java.util.Date;

import org.immutables.value.Value;

import com.lfp.joe.core.config.ValueLFP;

import at.favre.lib.bytes.Bytes;

@ValueLFP.Style
@Value.Immutable
public abstract class AbstractCaddyStorageValue {

	public abstract String key();

	public abstract Bytes value();

	public abstract Date modified();

}
