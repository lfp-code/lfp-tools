package com.lfp.tools.traefik.cert.manager.config;

import java.io.File;
import java.util.List;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.converter.FileConverter;
import com.lfp.joe.utils.Utils;

public interface TraefikCertManagerConfig extends Config {

	@DefaultValue("1")
	int version();

	@DefaultValue("traefik_cert_manager_${version}")
	String consulPrefix();

	@DefaultValue("/storage")
	@ConverterClass(FileConverter.class)
	File storageDir();

	@DefaultValue("/config")
	@ConverterClass(FileConverter.class)
	File configDir();

	@DefaultValue("_")
	String certificatesFileNameDelimiter();

	@DefaultValue("true")
	boolean generateWildcards();

	String acmeEmailAddress();

	List<String> domains();

	String zerosslKeyId();

	String zerosslMacKey();

	String cloudflareApiToken();

	@DefaultValue("CADDY_CLUSTERING_CONSUL_AESKEY")
	String caddyClusteringConsulAESKeyEnvName();

	default String consulAESKey() {
		return System.getenv(caddyClusteringConsulAESKeyEnvName());
	}

	public static void main(String[] args) {
		System.out.println(Utils.Crypto.getSecureRandomString(32));
		Configs.printProperties(PrintOptions.propertiesBuilder().withSkipPopulated(true).build());
	}

}
