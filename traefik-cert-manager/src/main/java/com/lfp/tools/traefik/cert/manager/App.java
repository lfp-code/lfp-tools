package com.lfp.tools.traefik.cert.manager;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.io.FileUtils;
import org.assimbly.docconverter.DocConverter;

import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.lfp.caddy.core.CaddyConfig;
import com.lfp.caddy.core.config.Apps;
import com.lfp.caddy.core.config.Automation;
import com.lfp.caddy.core.config.Certificates;
import com.lfp.caddy.core.config.Challenges;
import com.lfp.caddy.core.config.Dns;
import com.lfp.caddy.core.config.ExternalAccount;
import com.lfp.caddy.core.config.Issuer;
import com.lfp.caddy.core.config.Policy;
import com.lfp.caddy.core.config.Provider;
import com.lfp.caddy.core.config.Storage;
import com.lfp.caddy.core.config.Tls;
import com.lfp.caddy.run.Caddy;
import com.lfp.data.consul.AESEncryptedValue;
import com.lfp.data.consul.Consuls;
import com.lfp.data.consul.config.ConsulConfig;
import com.lfp.joe.core.function.Registration;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.process.Which;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.traefik.cert.manager.config.TraefikCertManagerConfig;
import com.lfp.tools.traefik.cert.manager.store.CaddyStorageValue;
import com.lfp.tools.traefik.cert.manager.store.CertificateContext;
import com.lfp.tools.traefik.cert.manager.store.CertificateContext.Metadata;
import com.lfp.tools.traefik.cert.manager.store.TraefikConfiguration;
import com.lfp.tools.traefik.cert.manager.store.TraefikConfiguration.Certificate;
import com.lfp.tools.traefik.cert.manager.store.TraefikConfiguration.TLS;
import com.orbitz.consul.KeyValueClient;
import com.orbitz.consul.cache.KVCache;
import com.orbitz.consul.model.kv.Value;

import at.favre.lib.bytes.Bytes;

public class App {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String NEW_LINE = "\n";

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		int exitValue;
		try {
			var caddyConfig = buildCaddyConfig();
			var process = Caddy.start(Which.get("caddy").get(), caddyConfig);
			try (var registration = monitorCertificates();) {
				exitValue = process.get().getExitValue();
			}
		} catch (Throwable t) {
			t.printStackTrace();
			exitValue = 1;
		}
		System.exit(exitValue);
	}

	private static Registration monitorCertificates() {
		var cfg = Configs.get(TraefikCertManagerConfig.class);
		var kvClient = Consuls.get().keyValueClient();
		var cache = KVCache.newCache(kvClient,
				cfg.consulPrefix(),
				1);
		Map<String, Date> contextsModified = new HashMap<>();
		cache.addListener(keyValues -> {
			var updated = Streams.of(keyValues.keySet()).filter(key -> {
				return List.of("crt", "key", "json").stream().anyMatch(ext -> hasExtension(key, ext));
			}).map(key -> {
				return Utils.Strings.substringBeforeLast(key, "/");
			}).distinct().map(path -> {
				try {
					return toCertificateContext(kvClient, path);
				} catch (Exception e) {
					LOGGER.warn("path parsing error:{}", path, e);
					return null;
				}
			})
					.nonNull()
					.sortedBy(v -> v.modified().getTime() * -1)
					.distinct(v -> v.name())
					.map(certificateContext -> {
						try {
							var lastModified = contextsModified.put(certificateContext.name(),
									certificateContext.modified());
							if (lastModified == null || lastModified.before(certificateContext.modified())) {
								updateCertificateContext(certificateContext);
								return true;
							}
						} catch (Exception e) {
							LOGGER.warn("path processing error:{}", certificateContext.path(), e);
						}
						return false;
					})
					.toSet()
					.contains(true);
			if (!updated)
				LOGGER.info("no changes found:{}", keyValues.keySet());
			System.gc();
		});
		cache.start();
		return cache::stop;
	}

	private static void updateCertificateContext(CertificateContext certificateContext) throws IOException {
		var cfg = Configs.get(TraefikCertManagerConfig.class);
		final var writeNamePrefix = certificateContext.name() + cfg.certificatesFileNameDelimiter();
		final var writeName = writeNamePrefix
				+ Utils.Crypto.getSecureRandomString();
		var writeDir = new FileExt(cfg.storageDir(), writeName);
		writeDir.mkdirs();
		var keyFile = write(writeDir, certificateContext, BeanRef.$(CertificateContext::key));
		var crtFile = write(writeDir, certificateContext, BeanRef.$(CertificateContext::crt));
		var configDir = cfg.configDir();
		configDir.mkdirs();
		var configFile = new FileExt(configDir,
				String.format("%s.yml",
						writeName));
		var traefikConfiguration = TraefikConfiguration.builder()
				.tls(TLS.builder().addCertificates(Certificate.of(keyFile, crtFile)).build())
				.build();
		var traefikConfigurationYaml = toYaml(traefikConfiguration);
		FileUtils.writeStringToFile(configFile, traefikConfigurationYaml, Utils.Bits.getDefaultCharset());
		for (var cleanupDir : List.of(writeDir.getParentFile(), configDir)) {
			for (var cleanupFile : Streams.of(cleanupDir.listFiles()).map(FileExt::from)) {
				if (!Utils.Strings.startsWith(cleanupFile.getName(), writeNamePrefix))
					continue;
				if (Arrays.asList(writeDir, configFile).stream().anyMatch(cleanupFile::equals))
					continue;
				FileUtils.forceDelete(cleanupFile);
			}
		}
		LOGGER.info("certificates updated. path:{} configFile:{} keyFile:{} crtFile:{}",
				certificateContext.path(),
				configFile.getAbsolutePath(),
				keyFile.getAbsolutePath(),
				crtFile.getAbsolutePath());
	}

	private static CertificateContext toCertificateContext(KeyValueClient kvClient, String path)
			throws InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException,
			BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
		var values = kvClient.getValues(path);
		CaddyStorageValue json, crt, key;
		if ((json = findCaddyStorageValue(values, "json")) == null)
			return null;
		if ((crt = findCaddyStorageValue(values, "crt")) == null)
			return null;
		if ((key = findCaddyStorageValue(values, "key")) == null)
			return null;
		var modified = Streams.of(json, crt, key)
				.map(CaddyStorageValue::modified)
				.reverseSorted()
				.findFirst()
				.get();
		var metadata = Serials.Gsons.fromBytes(json.value(), Metadata.class);
		return CertificateContext.builder()
				.path(path)
				.modified(modified)
				.metadata(metadata)
				.crt(crt.value())
				.key(key.value())
				.build();
	}

	private static CaddyConfig buildCaddyConfig() {
		var cfg = Configs.get(TraefikCertManagerConfig.class);
		var caddyConfigBuilder = CaddyConfig.builder();
		{
			var consulURI = Configs.get(ConsulConfig.class).uri();
			caddyConfigBuilder
					.storage(Storage.builder()
							.module("consul")
							.address(new InetSocketAddress(consulURI.getHost(), consulURI.getPort()))
							.prefix(cfg.consulPrefix())
							.aesKey(cfg.consulAESKey())
							.build());
		}
		var tlsBuilder = Tls.builder();
		{
			var domains = Streams.of(cfg.domains())
					.mapPartial(Utils.Strings::trimToNullOptional)
					.flatMap(domain -> {
						if (!cfg.generateWildcards())
							return Streams.of(domain);
						return Streams.of(domain.split("^\\*?\\."))
								.mapPartial(Utils.Strings::trimToNullOptional)
								.limit(1)
								.map(v -> String.format("*.%s", v))
								.prepend(domain);
					})
					.distinct()
					.toImmutableList();
			tlsBuilder.certificates(Certificates.builder().automate(domains).build());
		}
		{
			var issuerBuilder = Issuer.builder()
					.email(cfg.acmeEmailAddress())
					.module("acme")
					.challenges(Challenges.builder()
							.dns(Dns.builder()
									.provider(
											Provider.builder()
													.name("cloudflare")
													.apiToken(cfg.cloudflareApiToken())
													.build())
									.build())
							.build());
			if (Utils.Strings.isNotBlank(cfg.zerosslKeyId()) && Utils.Strings.isNotBlank(cfg.zerosslMacKey())) {
				issuerBuilder = issuerBuilder
						.ca("https://acme.zerossl.com/v2/DV90")
						.externalAccount(
								ExternalAccount.builder()
										.keyId(cfg.zerosslKeyId())
										.macKey(cfg.zerosslMacKey())
										.build());
			}
			tlsBuilder.automation(
					Automation.builder().policies(Policy.builder().issuers(issuerBuilder.build()).build()).build());
		}
		caddyConfigBuilder.apps(Apps.builder().tls(tlsBuilder.build()).build());
		return caddyConfigBuilder.build();
	}

	@SuppressWarnings("resource")
	private static CaddyStorageValue findCaddyStorageValue(List<Value> values, String ext)
			throws InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException,
			BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
		for (var value : Streams.of(values)
				.filter(v -> hasExtension(v.getKey(), ext))
				.filter(v -> v.getValueAsBytes().isPresent())) {
			return toCaddyStorageValue(value);
		}
		return null;
	}

	private static boolean hasExtension(final String key, String ext) {
		ext = Utils.Strings.trimToNull(ext);
		if (ext == null)
			return false;
		if (!Utils.Strings.startsWith(ext, "."))
			ext = "." + ext;
		return Utils.Strings.endsWithIgnoreCase(key, ext);
	}

	private static FileExt write(File writeDir, CertificateContext certificateContext,
			BeanPath<CertificateContext, Bytes> beanPath) throws IOException {
		var outFile = new FileExt(writeDir, certificateContext.name() + "." + beanPath.getPath());
		var contents = Utils.Strings.streamLines(beanPath.get(certificateContext).encodeUtf8())
				.filter(Utils.Strings::isNotBlank)
				.joining(NEW_LINE)
				+ NEW_LINE;
		try (var os = outFile.outputStream();
				var is = new ByteArrayInputStream(contents.getBytes(StandardCharsets.UTF_8))) {
			is.transferTo(os);
		}
		return outFile;
	}

	private static CaddyStorageValue toCaddyStorageValue(Value value)
			throws InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException,
			BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
		var cfg = Configs.get(TraefikCertManagerConfig.class);
		if (Utils.Strings.isNotBlank(cfg.consulAESKey()))
			value = AESEncryptedValue.of(cfg.consulAESKey(), value).decrypt();
		var json = value.getValueAsString().get();
		json = json.substring(json.indexOf("{"));
		json = json.substring(0, json.lastIndexOf("}") + 1);
		var jo = Serials.Gsons.getJsonParser().parse(json).getAsJsonObject();
		return CaddyStorageValue.builder()
				.key(value.getKey())
				.value(Bytes.parseBase64(jo.get("value").getAsString()))
				.modified(Utils.Times.tryParseDate(jo.get("modified").getAsString()).get())
				.build();
	}

	private static String toYaml(Object input) {
		var json = Serials.Gsons.get().toJson(input);
		var yaml = Throws.unchecked(() -> DocConverter.convertJsonToYaml(json));
		while (true) {
			var updated = Utils.Strings.trimToNull(Utils.Strings.removeStartIgnoreCase(yaml, "---"));
			if (yaml.equals(updated))
				break;
			yaml = updated;
		}
		yaml += "\n";
		return yaml;
	}

}
