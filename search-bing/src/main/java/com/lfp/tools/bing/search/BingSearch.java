package com.lfp.tools.bing.search;

import java.net.URI;
import java.time.Duration;
import java.util.List;
import java.util.Optional;

import com.google.re2j.Pattern;
import com.lfp.browser.playwright.client.BrowserContextLFP;
import com.lfp.browser.playwright.client.DOMs;
import com.lfp.browser.playwright.client.Pages;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.net.dns.TLDs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.bing.search.BingSearchService.BingPage;
import com.lfp.tools.search.service.SearchResult;
import com.lfp.tools.search.service.SearchService;
import com.microsoft.playwright.ElementHandle;
import com.microsoft.playwright.ElementHandle.PressOptions;
import com.microsoft.playwright.ElementHandle.TypeOptions;
import com.microsoft.playwright.JSHandle;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Page.NavigateOptions;
import com.microsoft.playwright.Page.ReloadOptions;
import com.microsoft.playwright.Page.WaitForFunctionOptions;
import com.microsoft.playwright.options.WaitUntilState;

import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;

public class BingSearch extends SearchService.Abs {
	private static final Duration TIMEOUT = Duration.ofSeconds(10);
	private static final TypeOptions TYPE_OPTIONS = new TypeOptions().setDelay(Duration.ofMillis(20).toMillis());
	private boolean lastPage;

	public BingSearch(BrowserContextLFP browserContext, String query) {
		super(browserContext, query);
	}

	@Override
	protected Iterable<SearchResult> getNextPage(long index) {
		if (this.lastPage)
			return List.of();
		if (index == 0)
			loadFirstPage();
		if (index > 0) {
			var page = getBrowserContext().getPage();
			var nextPageLinkOp = getNextPageLink(page);
			if (nextPageLinkOp.isEmpty())
				return List.of();
			try {
				nextPageLinkOp.get().click();
			} catch (Exception e) {
				page.evaluate("el=>el.click()", nextPageLinkOp.get());
			}
			// will cause wait for load
			if (getNextPageLink(page).isEmpty())
				this.lastPage = true;
		}
		return parseResults(index);
	}

	private Iterable<SearchResult> parseResults(long pageIndex) {
		return Pages.select(getBrowserContext().getPage(), "#b_content li.b_ad", "#b_content li.b_algo")
				.map(v -> toSearchResult(pageIndex, v));
	}

	private SearchResult toSearchResult(long pageIndex, ElementHandle elementHandle) {
		return new SearchResult.Abs() {

			@Override
			protected URI getURI(ElementHandle element) {
				var uri = super.getURI(element);
				if (uri == null)
					return uri;
				if (Utils.Strings.startsWith(uri.getPath(), "/shop"))
					return null;
				return uri;
			}

			@Override
			public long getPageIndex() {
				return pageIndex;
			}

			@Override
			public ElementHandle getElement() {
				return elementHandle;
			}

			@Override
			protected boolean isAdvertisement(ElementHandle element, URI uri) {
				String domainName = TLDs.parseDomainName(uri);
				if (!"bing.com".equalsIgnoreCase(domainName))
					return false;
				if (!Utils.Strings.startsWithIgnoreCase(uri.getPath(), "/aclk"))
					return false;
				return true;
			}

			@Override
			protected Iterable<ElementHandle> getVisitURIElements() {
				return DOMs.select(elementHandle, "a[href]");
			}

			@Override
			protected Page getPage() {
				return getBrowserContext().getPage();
			}

			@Override
			protected Iterable<URI> getImageURIsInternal() {
				return null;
			}

			@Override
			protected Iterable<String> getTitlesInternal() {
				return DOMs.select(elementHandle, "h2", "[class*=\"title\" i]").map(ElementHandle::innerText);
			}

			@Override
			protected Iterable<String> getDescriptionsInternal() {
				return DOMs.select(elementHandle, "p").map(ElementHandle::innerText);
			}
		};
	}

	private void loadFirstPage() {
		var page = getBrowserContext().getPage();
		page.navigate("https://www.bing.com/",
				new NavigateOptions().setWaitUntil(WaitUntilState.DOMCONTENTLOADED).setTimeout(TIMEOUT.toMillis()));
		var input = page.waitForSelector("#sb_form_q");
		input.click();
		input.type(getQuery(), TYPE_OPTIONS);
		input.press("Enter", new PressOptions().setNoWaitAfter(true));
		parseTotal(page);
	}

	private static long parseTotal(Page page) {
		var script = Utils.Resources.getResourceAsString("getTotal.js").get();
		JSHandle scriptResult = null;
		var attemptIter = IntStreamEx.range(3).iterator();
		while (scriptResult == null && attemptIter.hasNext()) {
			var attempt = attemptIter.next();
			try {
				if (attempt > 0)
					page.reload(new ReloadOptions().setTimeout(TIMEOUT.toMillis()));
				scriptResult = page.waitForFunction(script, null,
						new WaitForFunctionOptions().setTimeout(TIMEOUT.toMillis()));
			} catch (Exception e) {
				if (attemptIter.hasNext())
					continue;
				if (!MachineConfig.isDeveloper())
					throw Utils.Exceptions.asRuntimeException(e);
				var ss = Pages.screenshot(page);
				throw new RuntimeException("error screenshot:" + ss.getAbsolutePath(), e);
			}
		}
		var textStream = Utils.Lots.stream(scriptResult.getProperties()).values().map(v -> {
			return v.jsonValue();
		}).select(String.class);
		var whitespacePattern = Pattern.compile("\\s");
		textStream = textStream.map(v -> {
			var matcher = whitespacePattern.matcher(v);
			if (matcher.find())
				v = v.substring(0, matcher.start());
			return v;
		});
		textStream = textStream.map(v -> Utils.Strings.replace(v, ",", ""));
		var numStream = textStream.map(v -> Utils.Strings.parseNumber(v).orElse(null));
		numStream = numStream.nonNull();
		var longStream = numStream.mapToLong(v -> v.longValue());
		longStream = longStream.filter(v -> v > 0);
		var result = longStream.findFirst().orElse(0l);
		return result;
	}

	private static BingPage parseBingPage(Page page, long total) {
		return new BingPage() {

			@Override
			public EntryStream<URI, ElementHandle> streamAds() {
				return parseSelect(page, "#b_content li[class^=\"b_ad\"]", "li[class*=\" b_ad\"]");
			}

			@Override
			public EntryStream<URI, ElementHandle> streamResults() {
				return parseSelect(page, "#b_content li.b_algo");
			}

			@Override
			public long total() {
				return total;
			}
		};
	}

	private static EntryStream<URI, ElementHandle> parseSelect(Page page, String... selects) {
		var elStream = Pages.select(page, selects);
		elStream = elStream.distinct();
		var resultStream = elStream.mapToEntry(v -> getURI(page, v)).invert();
		resultStream = resultStream.nonNullKeys();
		resultStream = resultStream.distinctKeys();
		return resultStream;
	}

	private static Optional<ElementHandle> getNextPageLink(Page page) {
		var script = Utils.Resources.getResourceAsString("getNextButton.js").get();
		var functionResult = page.waitForFunction(script);
		var button = functionResult.asElement();
		return Optional.ofNullable(button);
	}

	private static URI getURI(Page page, ElementHandle el) {
		var uriStream = Utils.Lots.stream(el.querySelectorAll("h2 a[href]")).map(v -> {
			var uri = Pages.getAttributeAsURI(page, v, "href").orElse(null);
			return uri;
		});
		uriStream = uriStream.nonNull();
		uriStream = uriStream.map(v -> {
			return normalizeURI(page, v);
		});
		uriStream = uriStream.nonNull();
		return uriStream.findFirst().orElse(null);
	}

	private static URI normalizeURI(Page page, URI uri) {
		var domainName = TLDs.parseDomainName(uri);
		if (!"bing.com".equals(domainName))
			return uri;
		return URIs.streamQueryParameters(uri.getQuery()).filterKeys("url"::equals).values()
				.map(vv -> URIs.parse(vv).orElse(null)).nonNull().findFirst().orElse(uri);
	}

}
