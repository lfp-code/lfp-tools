package com.lfp.tools.bing.search;

import com.lfp.joe.core.lot.Lot; import com.lfp.joe.core.lot.AbstractLot; import java.net.URI;
import java.time.Duration;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;

import com.lfp.browser.playwright.client.BrowserContextLFP;
import com.lfp.browser.playwright.client.BrowserContexts;
import com.lfp.browser.playwright.client.Pages;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.net.dns.TLDs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.lot.AbstractLot;

import com.google.re2j.Pattern;
import com.microsoft.playwright.ElementHandle;
import com.microsoft.playwright.ElementHandle.PressOptions;
import com.microsoft.playwright.ElementHandle.TypeOptions;
import com.microsoft.playwright.JSHandle;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Page.NavigateOptions;
import com.microsoft.playwright.Page.ReloadOptions;
import com.microsoft.playwright.Page.WaitForFunctionOptions;
import com.microsoft.playwright.options.WaitUntilState;

import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public enum BingSearchService implements BiFunction<BrowserContextLFP, String, Iterator<BingSearchService.BingPage>> {
	INSTANCE;

	private static final TypeOptions TYPE_OPTIONS = new TypeOptions().setDelay(Duration.ofMillis(20).toMillis());
	private static final String FOLLOW_REDIRECTS_SCRIPT = "(url)=>{try{return fetch(url).then(response=>response.url).catch(e=>null);}catch(e){return null;}}";
	private static final Duration TIMEOUT = Duration.ofSeconds(10);

	/**
	 *
	 */
	@Override
	public Iterator<BingPage> apply(BrowserContextLFP browserContext, String query) {
		query = Optional.ofNullable(query).orElse("");
		var page = browserContext.getPage();
		page.navigate("https://www.bing.com/",
				new NavigateOptions().setWaitUntil(WaitUntilState.DOMCONTENTLOADED).setTimeout(TIMEOUT.toMillis()));
		var input = page.waitForSelector("#sb_form_q");
		input.click();
		input.type(query, TYPE_OPTIONS);
		input.press("Enter", new PressOptions().setNoWaitAfter(true));
		return createIterator(browserContext);

	}

	private static Iterator<BingPage> createIterator(BrowserContextLFP browserContext) {
		var page = browserContext.getPage();
		var total = parseTotal(page);
		var iter = new AbstractLot.Indexed<BingPage>() {

			private final Set<URI> uniqueTracker = Utils.Lots.newConcurrentHashSet();

			@Override
			protected BingPage computeNext(long index) {
				boolean finalBingPage = false;
				if (index > 0) {
					var nextPageLinkOp = getNextPageLink(page);
					if (nextPageLinkOp.isEmpty())
						return this.end();
					try {
						nextPageLinkOp.get().click();
					} catch (Exception e) {
						page.evaluate("el=>el.click()", nextPageLinkOp.get());
					}
					// will cause wait for load
					if (getNextPageLink(page).isEmpty())
						finalBingPage = true;
				}
				var bingPage = parseBingPage(page, total);
				if (bingPage.isEmpty())
					return this.end();
				if (finalBingPage)
					return this.end(bingPage);
				boolean newURIFound = false;
				for (var uri : bingPage.streamResults().keys())
					if (uniqueTracker.add(uri))
						newURIFound = true;
				if (!newURIFound)
					return this.end(bingPage);
				return bingPage;
			}
		};
		return iter;
	}

	private static long parseTotal(Page page) {
		var script = Utils.Resources.getResourceAsString("getTotal.js").get();
		JSHandle scriptResult = null;
		var attemptIter = IntStreamEx.range(3).iterator();
		while (scriptResult == null && attemptIter.hasNext()) {
			var attempt = attemptIter.next();
			try {
				if (attempt > 0)
					page.reload(new ReloadOptions().setTimeout(TIMEOUT.toMillis()));
				scriptResult = page.waitForFunction(script, null,
						new WaitForFunctionOptions().setTimeout(TIMEOUT.toMillis()));
			} catch (Exception e) {
				if (attemptIter.hasNext())
					continue;
				if (!MachineConfig.isDeveloper())
					throw Utils.Exceptions.asRuntimeException(e);
				var ss = Pages.screenshot(page);
				throw new RuntimeException("error screenshot:" + ss.getAbsolutePath(), e);
			}
		}
		var textStream = Utils.Lots.stream(scriptResult.getProperties()).values().map(v -> {
			return v.jsonValue();
		}).select(String.class);
		var whitespacePattern = Pattern.compile("\\s");
		textStream = textStream.map(v -> {
			var matcher = whitespacePattern.matcher(v);
			if (matcher.find())
				v = v.substring(0, matcher.start());
			return v;
		});
		textStream = textStream.map(v -> Utils.Strings.replace(v, ",", ""));
		var numStream = textStream.map(v -> Utils.Strings.parseNumber(v).orElse(null));
		numStream = numStream.nonNull();
		var longStream = numStream.mapToLong(v -> v.longValue());
		longStream = longStream.filter(v -> v > 0);
		var result = longStream.findFirst().orElse(0l);
		return result;
	}

	private static BingPage parseBingPage(Page page, long total) {
		return new BingPage() {

			@Override
			public EntryStream<URI, ElementHandle> streamAds() {
				return parseSelect(page, "#b_content li[class^=\"b_ad\"]", "#b_content li[class*=\" b_ad\"]");
			}

			@Override
			public EntryStream<URI, ElementHandle> streamResults() {
				return parseSelect(page, "#b_content li.b_algo");
			}

			@Override
			public long total() {
				return total;
			}
		};
	}

	private static EntryStream<URI, ElementHandle> parseSelect(Page page, String... selects) {
		var elStream = Pages.select(page, selects);
		elStream = elStream.distinct();
		var resultStream = elStream.mapToEntry(v -> getURI(page, v)).invert();
		resultStream = resultStream.nonNullKeys();
		resultStream = resultStream.distinctKeys();
		return resultStream;
	}

	private static Optional<ElementHandle> getNextPageLink(Page page) {
		var script = Utils.Resources.getResourceAsString("getNextButton.js").get();
		var functionResult = page.waitForFunction(script);
		var button = functionResult.asElement();
		return Optional.ofNullable(button);
	}

	private static URI getURI(Page page, ElementHandle el) {
		var uriStream = Utils.Lots.stream(el.querySelectorAll("h2 a[href]")).map(v -> {
			var uri = Pages.getAttributeAsURI(page, v, "href").orElse(null);
			return uri;
		});
		uriStream = uriStream.nonNull();
		uriStream = uriStream.map(v -> {
			return normalizeURI(page, v);
		});
		uriStream = uriStream.nonNull();
		return uriStream.findFirst().orElse(null);
	}

	private static URI normalizeURI(Page page, URI uri) {
		var domainName = TLDs.parseDomainName(uri);
		if (!"bing.com".equals(domainName))
			return uri;
		if (URIs.pathPrefixMatch(uri.getPath(), "/aclk")) {
			var js = FOLLOW_REDIRECTS_SCRIPT;
			var response = page.evaluate(js, uri.toString());
			return Utils.Types.tryCast(response, String.class).map(v -> URIs.parse(v).orElse(null)).orElse(null);
		}
		return URIs.streamQueryParameters(uri.getQuery()).filterKeys("url"::equals).values()
				.map(vv -> URIs.parse(vv).orElse(null)).nonNull().findFirst().orElse(null);
	}

	public static interface BingPage {

		EntryStream<URI, ElementHandle> streamAds();

		EntryStream<URI, ElementHandle> streamResults();

		long total();

		default EntryStream<URI, ElementHandle> streamAll() {
			var stream = Utils.Lots.flatMap(Arrays.asList(streamAds(), streamResults()));
			stream = stream.nonNull();
			return stream.mapToEntry(Entry::getKey, Entry::getValue);
		}

		default boolean isEmpty() {
			return !streamAll().iterator().hasNext();
		}

	}

	public static void main(String[] args) {
		try (var bc = BrowserContexts.launch()) {
			var results = BingSearchService.INSTANCE.apply(bc, "instagram nfl 88");
			int index = -1;
			while (results.hasNext()) {
				index++;
				var bp = results.next();
				Map<EntryStream<URI, ElementHandle>, Boolean> map = Map.of(bp.streamAds(), true, bp.streamResults(),
						false);
				for (var ent : map.entrySet()) {
					for (var resultEnt : ent.getKey())
						System.out.println(StreamEx.of(index, ent.getValue(), resultEnt.getKey()).joining("\t"));
				}
			}
		}
	}

}