() => {
	for (var el of document.querySelectorAll("#b_results .b_no")) {
		var innerText = el.innerText;
		if (utz.containsIgnoreCase(innerText, "no results"))
			return "#";
	}
	for (var el of document.querySelectorAll("nav[role=\"navigation\"] a.sb_pagN[href]")) {
		if (!el.isConnected)
			continue;
		var url = utz.toURL(el.href)
		if (url != null)
			return el;
	}
	return null;
};