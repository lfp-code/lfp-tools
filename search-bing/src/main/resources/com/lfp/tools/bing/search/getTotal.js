() => {
	for (var el of document.querySelectorAll("#b_results .b_no")) {
		var innerText = el.innerText;
		if (utz.containsIgnoreCase(innerText, "no results"))
			return [];
	}
	var results = [];
	for (var el of document.querySelectorAll("#b_content .sb_count")) {
		if (!el.isConnected)
			continue;
		var innerText = el.innerText;
		if (!utz.isBlank(innerText) && /\d/.test(innerText))
			results.push(innerText);
	}
	if (results.length > 0)
		return results;
}