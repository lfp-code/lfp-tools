package com.lfp.tools.search.service;

import com.lfp.joe.core.lot.Lot; import com.lfp.joe.core.lot.AbstractLot; import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import com.lfp.browser.playwright.client.BrowserContextLFP;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.lot.AbstractLot;

import one.util.streamex.StreamEx;

public interface SearchService extends Iterator<SearchResult> {

	public static abstract class Abs implements SearchService {

		private final Set<Object> disntinctTracker = Utils.Lots.newConcurrentHashSet();
		private final BrowserContextLFP browserContext;
		private final String normalizedQuery;
		private Iterator<SearchResult> delegate;

		public Abs(BrowserContextLFP browserContext, String query) {
			this.browserContext = Objects.requireNonNull(browserContext);
			this.normalizedQuery = normalizeQuery(query);
			var pageIterator = new AbstractLot.Indexed<StreamEx<SearchResult>>() {

				private boolean newDataFound = true;

				@Override
				protected StreamEx<SearchResult> computeNext(long index) {
					if (!newDataFound)
						return this.end();
					if (Utils.Strings.isBlank(normalizedQuery))
						return this.end();
					this.newDataFound = false;
					var next = getNextPage(index);
					var nextStream = normalizePage(next);
					nextStream = nextStream.filter(v -> {
						return Utils.Lots.hasNext(v.streamVisitEntries()).getKey();
					});
					nextStream = nextStream.map(v -> {
						if (isNewSearchResult(disntinctTracker, v))
							this.newDataFound = true;
						return v;
					});
					var hasNext = Utils.Lots.hasNext(nextStream);
					if (!hasNext.getKey())
						return this.end();
					return hasNext.getValue();
				}
			};
			this.delegate = Utils.Lots.flatMap(Utils.Lots.stream(pageIterator)).iterator();
		}

		@Override
		public SearchResult next() {
			return delegate.next();
		}

		@Override
		public boolean hasNext() {
			return delegate.hasNext();
		}

		protected BrowserContextLFP getBrowserContext() {
			return browserContext;
		}

		protected String getQuery() {
			return normalizedQuery;
		}

		protected String normalizeQuery(String query) {
			return Utils.Strings.lowerCase(Utils.Strings.trimToNull(query));
		}

		protected StreamEx<SearchResult> normalizePage(Iterable<SearchResult> ible) {
			var stream = Utils.Lots.stream(ible);
			stream = stream.nonNull();
			return stream;
		}

		protected boolean isNewSearchResult(Set<Object> distinctTracker, SearchResult searchResult) {
			if (searchResult.isAdvertisement())
				return false;
			boolean result = false;
			for (var uri : searchResult.streamVisitEntries().values())
				if (distinctTracker.add(uri))
					result = true;
			return result;
		}

		protected abstract Iterable<SearchResult> getNextPage(long index);

	}

}