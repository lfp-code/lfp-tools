package com.lfp.tools.search.service;

import java.net.URI;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import com.lfp.browser.playwright.client.Pages;
import com.lfp.joe.utils.Utils;

import com.microsoft.playwright.ElementHandle;
import com.microsoft.playwright.Page;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public interface SearchResult {

	long getPageIndex();

	Optional<String> getTitle();

	Optional<String> getDescription();

	ElementHandle getElement();

	StreamEx<URI> streamImageURIs();

	default EntryStream<ElementHandle, URI> streamVisitEntries() {
		return streamVisitEntries(false);
	}

	EntryStream<ElementHandle, URI> streamVisitEntries(boolean followRedirects);

	boolean isAdvertisement();

	public static abstract class Abs implements SearchResult {

		private final Map<Boolean, Iterable<Entry<ElementHandle, URI>>> visitCache = new ConcurrentHashMap<>();

		protected abstract Page getPage();

		@Override
		public Optional<String> getTitle() {
			return concat(getTitlesInternal());
		}

		protected abstract Iterable<String> getTitlesInternal();

		@Override
		public Optional<String> getDescription() {
			return concat(getDescriptionsInternal());
		}

		protected abstract Iterable<String> getDescriptionsInternal();

		@Override
		public EntryStream<ElementHandle, URI> streamVisitEntries(boolean followRedirects) {
			if (!followRedirects) {
				var ible = this.visitCache.computeIfAbsent(false, nil -> {
					var elStream = Utils.Lots.stream(getVisitURIElements());
					elStream = elStream.nonNull();
					var estream = elStream.mapToEntry(v -> getURI(v));
					estream = estream.nonNullValues();
					return Utils.Lots.asCollection(estream);
				});
				return Utils.Lots.streamEntries(ible);
			}
			var ible = this.visitCache.computeIfAbsent(true, nil -> {
				var estream = streamVisitEntries(false);
				estream = estream.map(ent -> {
					if (!shouldFollowRedirect(ent.getKey(), ent.getValue()))
						return ent;
					var uri = ent.getValue();
					uri = Pages.followRedirect(getPage(), uri).orElse(uri);
					return Utils.Lots.entry(ent.getKey(), uri);
				}).mapToEntry(Entry::getKey, Entry::getValue);
				return Utils.Lots.asCollection(estream);
			});
			return Utils.Lots.streamEntries(ible);
		}

		protected abstract Iterable<ElementHandle> getVisitURIElements();

		protected URI getURI(ElementHandle element) {
			if (element == null)
				return null;
			return Pages.getAttributeAsURI(getPage(), element, "href").orElse(null);
		}

		@Override
		public StreamEx<URI> streamImageURIs() {
			return Utils.Lots.stream(getImageURIsInternal()).nonNull().distinct();
		}

		protected abstract Iterable<URI> getImageURIsInternal();

		@Override
		public boolean isAdvertisement() {
			var estream = Utils.Lots.flatMap(StreamEx.of(false, true).map(v -> streamVisitEntries(v)))
					.mapToEntry(Entry::getKey, Entry::getValue);
			estream = estream.distinctValues();
			return estream.anyMatch((k, v) -> {
				return isAdvertisement(k, v);
			});
		}

		protected boolean shouldFollowRedirect(ElementHandle element, URI uri) {
			return isAdvertisement(element, uri);
		}

		protected abstract boolean isAdvertisement(ElementHandle element, URI uri);

		private static Optional<String> concat(Iterable<String> ible) {
			var stream = Utils.Lots.stream(ible);
			stream = stream.nonNull();
			stream = stream.map(Utils.Strings::trim);
			stream = stream.distinct(v -> v.toLowerCase());
			var result = stream.joining(" ");
			if (result.isEmpty())
				return Optional.empty();
			return Optional.of(result);
		}

	}

}
