package com.lfp.tools.html.summary.service;

import java.io.IOException;
import java.net.URI;

import com.lfp.joe.net.proxy.Proxy;

public interface HtmlSummaryService {

	HtmlSummaryServiceResponse apply(URI uri, Proxy proxy) throws IOException;

	HtmlSummaryServiceResponse apply(String html);

}
