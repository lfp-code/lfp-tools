package test;

import com.lfp.browser.playwright.client.BrowserContexts;
import com.lfp.tools.search.duckduckgo.DuckDuckGoSearch;

import one.util.streamex.StreamEx;

public class DuckDuckGoSearchTest {

	public static void main(String[] args) {
		try (var bc = BrowserContexts.launch()) {
			var iter = new DuckDuckGoSearch(bc, "hats");
			int index = -1;
			while (iter.hasNext()) {
				index++;
				var result = iter.next();
				System.out.println(StreamEx.of(index, result.getPageIndex(), result.isAdvertisement(),
						result.getTitle().orElse(null), result.streamVisitEntries().values().findFirst().orElse(null))
						.joining("\t"));
			}
		}

	}
}
