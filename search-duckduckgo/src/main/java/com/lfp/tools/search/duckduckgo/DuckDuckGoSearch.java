package com.lfp.tools.search.duckduckgo;

import java.net.URI;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.lfp.browser.playwright.client.BrowserContextLFP;
import com.lfp.browser.playwright.client.DOMs;
import com.lfp.browser.playwright.client.Pages;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.net.dns.TLDs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.search.service.SearchResult;
import com.lfp.tools.search.service.SearchService;
import com.microsoft.playwright.ElementHandle;
import com.microsoft.playwright.ElementHandle.PressOptions;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Page.NavigateOptions;
import com.microsoft.playwright.options.LoadState;
import com.microsoft.playwright.options.WaitUntilState;

import reactor.core.publisher.Mono;

public class DuckDuckGoSearch extends SearchService.Abs {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private boolean lastPage;

	public DuckDuckGoSearch(BrowserContextLFP browserContext, String query) {
		super(browserContext, query);
	}

	@Override
	protected Iterable<SearchResult> getNextPage(long index) {
		if (lastPage)
			return List.of();
		if (index == 0)
			loadFirstPage();
		else {
			var page = getBrowserContext().getPage();
			var nextSelector = "input[type=\"submit\"][value=\"next\" i]";
			var next = page.querySelector(nextSelector);
			if (next == null)
				return List.of();
			next.click();
			page.waitForLoadState(LoadState.DOMCONTENTLOADED);
			lastPage = lastPage || !this.getBrowserContext().poll(Duration.ofMillis(250), nil -> {
				var nextNext = page.querySelector(nextSelector);
				return nextNext != null;
			}).timeout(Duration.ofSeconds(10)).onErrorResume(t -> t instanceof TimeoutException, t -> Mono.just(false))
					.blockFirst();
		}
		return parseResultPage(index);
	}

	private Iterable<SearchResult> parseResultPage(long index) {
		var page = getBrowserContext().getPage();
		var elStream = Pages.select(page, "#links .result");
		var resultStream = elStream.map(v -> {
			return createSearchResult(index, v);
		});
		return resultStream;
	}

	private SearchResult createSearchResult(long index, ElementHandle element) {
		return new SearchResult.Abs() {

			@Override
			public long getPageIndex() {
				return index;
			}

			@Override
			public ElementHandle getElement() {
				return element;
			}

			@Override
			protected boolean isAdvertisement(ElementHandle element, URI uri) {
				String domainName = TLDs.parseDomainName(uri);
				if (!"duckduckgo.com".equalsIgnoreCase(domainName))
					return false;
				var values = URIs.streamQueryParameters(uri.getQuery()).filterKeys("ad_provider"::equals).values();
				values = values.filter(Utils.Strings::isNotBlank);
				if (values.iterator().hasNext())
					return true;
				return false;
			}

			@Override
			protected Iterable<ElementHandle> getVisitURIElements() {
				return element.querySelectorAll("a[class*=\"_a\"]");
			}

			@Override
			protected Page getPage() {
				return getBrowserContext().getPage();
			}

			@Override
			protected Iterable<URI> getImageURIsInternal() {
				return null;
			}

			@Override
			protected Iterable<String> getTitlesInternal() {
				return DOMs.select(element, "[class*=\"_title\"]").map(v -> v.innerText());
			}

			@Override
			protected Iterable<String> getDescriptionsInternal() {
				return DOMs.select(element, "[class*=\"_snippet\"]").map(v -> v.innerText());
			}

		};
	}

	private boolean loadFirstPage() {
		var page = this.getBrowserContext().getPage();
		page.navigate("https://html.duckduckgo.com/",
				new NavigateOptions().setWaitUntil(WaitUntilState.DOMCONTENTLOADED));
		var input = page.waitForSelector("#search_form_input_homepage");
		input.type(this.getQuery());
		input.press("Enter", new PressOptions().setNoWaitAfter(true));
		var script = "()=>{if(!!document.querySelector('#links .no-results')){return '#'}for(var el of document.querySelectorAll('#links .result a[href]')){return el}}";
		var scriptResult = page.waitForFunction(script);
		var resultFound = scriptResult.asElement() != null;
		if (!resultFound && MachineConfig.isDeveloper()) {
			var doc = Pages.html(page);
			var ss = Pages.screenshot(page);
			logger.warn("first page load failed. query:{} screen-shot:{} html:\n{}", this.getQuery(),
					ss.getAbsolutePath(), doc.outerHtml());
			return false;
		}
		return resultFound;
	}

}
