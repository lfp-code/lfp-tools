package com.lfp.tools.file.converter.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpResponse;

import com.google.common.net.MediaType;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.media.MediaTypes;
import com.lfp.joe.utils.bytes.InputStreamSource;

public interface FileConverterService {

	default InputStream convert(URI uri, OutputFileType outputFileType) throws IOException {
		if (uri == null)
			return convert((InputStreamSource) null, outputFileType);
		ThrowingSupplier<HttpResponse<InputStream>, IOException> responseSupplier = () -> {
			var request = HttpRequests.request().uri(uri);
			return HttpClients.send(request);
		};
		MediaType mediaType;
		var response = responseSupplier.get();
		try (var is = response.body()) {
			mediaType = MediaTypes.tryParse(HeaderMap.of(response.headers())).orElse(null);
		}
		return convert(InputStreamSource.create(() -> responseSupplier.get().body()), mediaType, outputFileType);
	}

	default InputStream convert(File file, OutputFileType outputFileType) throws IOException {
		return convert(InputStreamSource.create(file), MediaTypes.tryParse(file).orElse(null), outputFileType);
	}

	default InputStream convert(InputStreamSource source, OutputFileType outputFileType) throws IOException {
		return convert(source, null, outputFileType);
	}

	InputStream convert(InputStreamSource source, MediaType mediaType, OutputFileType outputFileType)
			throws IOException;

}
