package com.lfp.tools.file.converter.service;

import com.google.common.net.MediaType;

public enum OutputFileType {
	JPEG {
		@Override
		public MediaType getMediaType() {
			return MediaType.JPEG;
		}

		@Override
		public boolean isImage() {
			return true;
		}
	},
	PNG {
		@Override
		public MediaType getMediaType() {
			return MediaType.PNG;
		}

		@Override
		public boolean isImage() {
			return true;
		}
	},
	PDF {
		@Override
		public MediaType getMediaType() {
			return MediaType.PDF;
		}

		@Override
		public boolean isImage() {
			return false;
		}
	};

	public abstract MediaType getMediaType();

	public abstract boolean isImage();
}
