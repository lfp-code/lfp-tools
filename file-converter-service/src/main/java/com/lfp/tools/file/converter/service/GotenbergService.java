package com.lfp.tools.file.converter.service;

import java.util.List;
import java.util.Optional;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.media.MediaTypes;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.bytes.InputStreamSource;
import com.lfp.tools.file.converter.service.config.GotenbergServiceConfig;

import com.google.common.net.MediaType;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface GotenbergService {

	public static GotenbergServiceConfig config() {
		return Configs.get(GotenbergServiceConfig.class);
	}

	public static enum ConversionSource {
		HTML, URL, Office, Markdown;

		@Override
		public String toString() {
			return super.toString().toLowerCase();
		}

		public static Optional<ConversionSource> tryParse(InputStreamSource iss, MediaType mediaType) {
			if (iss == null && mediaType == null)
				return Optional.empty();
			if (mediaType == null)
				mediaType = MediaTypes.tryParse(iss).orElse(null);
			if (mediaType == null)
				return Optional.empty();
			if ("text".equalsIgnoreCase(mediaType.type())) {
				if (Utils.Strings.containsIgnoreCase(mediaType.subtype(), "html"))
					return Optional.of(ConversionSource.HTML);
				if (Utils.Strings.containsIgnoreCase(mediaType.subtype(), "markdown"))
					return Optional.of(ConversionSource.Markdown);
			}
			if ("application".equalsIgnoreCase(mediaType.type())) {
				if (Utils.Strings.containsIgnoreCase(mediaType.subtype(), "ooxml"))
					return Optional.of(ConversionSource.Office);
				if (Utils.Strings.containsIgnoreCase(mediaType.subtype(), "office"))
					return Optional.of(ConversionSource.Office);
			}
			return Optional.empty();
		}
	}

	@Multipart
	@POST("/convert/{conversionSource}")
	Call<ResponseBody> convert(@Path(value = "conversionSource", encoded = true) ConversionSource conversionSource,
			@Part List<MultipartBody.Part> files);
}
