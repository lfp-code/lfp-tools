package com.lfp.tools.robots.txt.lookup.impl;

import java.net.URI;
import java.time.Duration;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;

import com.google.common.reflect.TypeToken;
import com.lfp.data.redis.client.codec.GsonCodec;
import com.lfp.data.redis.client.commands.SyncCommands;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.ssdb.client.SSDBClients;
import com.lfp.tools.robots.txt.lookup.service.RobotsTxtRecord;

public class SSDBRobotsTxtLookupService extends AbstractRobotsTxtLookupService {

	private static final long DEFAULT_MAX_MEMORY_CACHE_SIZE = 10_000;
	private static final Duration DEFAULT_CACHE_DURATION = Duration.ofHours(1);
	@SuppressWarnings("serial")
	public static MemoizedSupplier<SSDBRobotsTxtLookupService> INSTANCE_S = Utils.Functions.memoize(() -> {
		var codec = new GsonCodec<String, Optional<RobotsTxtRecord>>(Serials.Gsons.get(), TypeToken.of(String.class),
				new TypeToken<Optional<RobotsTxtRecord>>() {
				});
		return new SSDBRobotsTxtLookupService(SSDBClients.createSync(codec), DEFAULT_MAX_MEMORY_CACHE_SIZE,
				DEFAULT_CACHE_DURATION);
	});

	public static SSDBRobotsTxtLookupService getDefault() {
		return INSTANCE_S.get();
	}

	private final SyncCommands<String, Optional<RobotsTxtRecord>> ssdbClient;

	public SSDBRobotsTxtLookupService(SyncCommands<String, Optional<RobotsTxtRecord>> ssdbClient, long memoryCacheSize,
			Duration cacheDuration) {
		super(memoryCacheSize, cacheDuration);
		this.ssdbClient = Objects.requireNonNull(ssdbClient);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Optional<RobotsTxtRecord> cacheRead(Entry<URI, Proxy> candidate, Duration cacheDuration) {
		var cacheKey = getCacheKey(candidate, cacheDuration);
		var value = this.ssdbClient.get(cacheKey);
		return Utils.Types.tryCast(value, Optional.class).orElse(null);
	}

	@Override
	protected void cacheWrite(Entry<URI, Proxy> candidate, Duration cacheDuration, Optional<RobotsTxtRecord> valueOp) {
		var cacheKey = getCacheKey(candidate, cacheDuration);
		if (valueOp == null) {
			this.ssdbClient.del(cacheKey);
			return;
		}
		this.ssdbClient.setex(cacheKey, cacheDuration.toSeconds(), valueOp);

	}

	public static void main(String[] args) {
		var service = SSDBRobotsTxtLookupService.getDefault();
		var result = service.apply("ww.aa.com");
		System.out.println(Serials.Gsons.getPretty().toJson(result));
	}
}
