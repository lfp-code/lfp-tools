package com.lfp.tools.robots.txt.lookup.impl;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import com.lfp.joe.net.dns.TLDs;
import com.lfp.joe.net.http.headers.UserAgents;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.cookie.CookieJars;
import com.lfp.joe.okhttp.interceptor.MetaRefreshRedirectInterceptor;
import com.lfp.joe.okhttp.interceptor.UserAgentInterceptor;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.lookup.service.AbstractLookupService;
import com.lfp.tools.robots.txt.lookup.service.RobotsTxtLookupRequest;
import com.lfp.tools.robots.txt.lookup.service.RobotsTxtLookupService;
import com.lfp.tools.robots.txt.lookup.service.RobotsTxtRecord;
import com.panforge.robotstxt.RobotsTxt;

import de.xn__ho_hia.storage_unit.StorageUnit;
import de.xn__ho_hia.storage_unit.StorageUnits;
import io.mikael.urlbuilder.UrlBuilder;
import okhttp3.Response;

public abstract class AbstractRobotsTxtLookupService
		extends AbstractLookupService<RobotsTxtLookupRequest, Entry<URI, Proxy>, RobotsTxtRecord>
		implements RobotsTxtLookupService {
	private static final int VERSION = 0;
	private static final StorageUnit<?> MAX_STREAM_SIZE = StorageUnits.kilobyte(128);

	public AbstractRobotsTxtLookupService(long memoryCacheSize, Duration cacheDuration) {
		super(memoryCacheSize, cacheDuration);
	}

	@Override
	protected List<Entry<URI, Proxy>> getCandidates(RobotsTxtLookupRequest request) {
		String lookup = request == null ? null : request.getLookup();
		lookup = Utils.Strings.trimToNull(lookup);
		if (lookup == null)
			return List.of();
		Set<URI> uris = new LinkedHashSet<URI>();
		{
			URIs.parse(lookup).ifPresent(uris::add);
			if (uris.isEmpty()) {
				URI uri = URIs.parse("https://" + lookup).orElse(null);
				if (uri != null) {
					uris.add(uri);
					uris.add(UrlBuilder.fromUri(uri).withScheme("http").toUri());
				}
			}
		}
		if (uris.isEmpty())
			return List.of();
		for (var modURI : new ArrayList<>(uris)) {
			var domain = TLDs.parseDomainName(modURI);
			if (Utils.Strings.isNotBlank(domain) && !Utils.Strings.equalsIgnoreCase(domain, modURI.getHost()))
				uris.add(UrlBuilder.fromUri(modURI).withHost(domain).toUri());
		}
		uris = Utils.Lots.stream(uris).map(v -> {
			return UrlBuilder.fromUri(v).withPath("/robots.txt").toUri();
		}).toCollection(LinkedHashSet::new);
		return Utils.Lots.stream(uris).mapToEntry(v -> v, v -> request.getProxy()).toImmutableList();
	}

	@Override
	protected String getCacheKey(Entry<URI, Proxy> candidate, Duration cacheDuration) {
		return "robots_txt_" + Utils.Crypto
				.hashMD5(VERSION, this.getClass(), cacheDuration.toSeconds(), candidate.getKey()).encodeHex();
	}

	@Override
	protected Optional<RobotsTxtRecord> lookupFresh(Entry<URI, Proxy> candidate) {
		var cb = Ok.Clients.get(candidate.getValue()).newBuilder();
		cb.cookieJar(CookieJars.create());
		cb.addInterceptor(UserAgentInterceptor.getBrowserDefault());
		cb.addInterceptor(new MetaRefreshRedirectInterceptor());
		Optional<RobotsTxt> robotsTxtOp;
		try (var rctx = Ok.Calls.execute(cb.build(), candidate.getKey())) {
			robotsTxtOp = rctx.parse(r -> parseRobotsText(r)).getParsedBody();
		} catch (IOException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
		if (robotsTxtOp.isEmpty())
			return Optional.empty();
		var result = RobotsTxtRecord.builder().uri(candidate.getKey()).value(robotsTxtOp.get().toString()).build();
		return Optional.of(result);
	}

	private static Optional<RobotsTxt> parseRobotsText(Response response) throws IOException {
		if (!response.isSuccessful())
			return Optional.empty();
		if (!isValidContentType(response))
			return Optional.empty();
		if (!isValidPath(response))
			return Optional.empty();
		RobotsTxt robotsTxt;
		try (var is = Utils.Bits.limit(response.body().byteStream(), MAX_STREAM_SIZE.longValue());) {
			robotsTxt = RobotsTxt.read(is);
		}
		if (Utils.Strings.isBlank(robotsTxt.toString()))
			return Optional.empty();
		// test
		robotsTxt.ask(UserAgents.CHROME_LATEST.get(), "/");
		return Optional.of(robotsTxt);
	}

	private static boolean isValidPath(Response response) {
		var path = response.request().url().uri().getPath();
		path = Utils.Strings.stripEnd(path, "/");
		path = Utils.Strings.stripStart(path, "/");
		if (Utils.Strings.equalsIgnoreCase(path, "robots.txt"))
			return true;
		return false;
	}

	private static boolean isValidContentType(Response response) {
		var contentTypeOp = Ok.Calls.getContentType(response);
		if (contentTypeOp.isEmpty())
			return true;
		var mediaType = contentTypeOp.get();
		if (!Utils.Strings.equalsIgnoreCase(mediaType.type(), "text"))
			return false;
		var subtype = mediaType.subtype();
		if (Utils.Strings.isBlank(subtype))
			return true;
		return "plain".equalsIgnoreCase(subtype);
	}

}
