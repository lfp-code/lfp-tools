var fs = require('fs');
var summarize = require('summarize');
var data = '%s';
try {
	var stats = fs.statSync(data);
	if (stats.isFile())
		data = fs.readFileSync(data, 'utf8');
} catch (e) {
	//supress
}
try {
	return summarize(data);
} catch (e) {
	return {
		ok: false,
		error: e.toString()
	};
}