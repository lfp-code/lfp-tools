package com.lfp.tools.html.summary.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.Objects;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.time.StopWatch;

import com.lfp.code.bridge.nodejs.NodeJSEvalService;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.cookie.CookieJars;
import com.lfp.joe.okhttp.interceptor.MetaRefreshRedirectInterceptor;
import com.lfp.joe.okhttp.interceptor.UserAgentInterceptor;
import com.lfp.joe.utils.Utils;
import com.lfp.proxy.routing.client.ProxyRoutingClient;
import com.lfp.tools.html.summary.service.HtmlSummaryService;
import com.lfp.tools.html.summary.service.HtmlSummaryServiceResponse;

public class HtmlSummaryServiceImpl extends NodeJSEvalService implements HtmlSummaryService {

	public static final HtmlSummaryServiceImpl INSTANCE = Utils.Functions.unchecked(() -> new HtmlSummaryServiceImpl());

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private final String template;

	protected HtmlSummaryServiceImpl() throws IOException {
		super("summarize");
		this.template = Utils.Resources.getResourceAsString("script.js").get();
	}

	@Override
	public HtmlSummaryServiceResponse apply(URI uri, Proxy proxy) throws IOException {
		Objects.requireNonNull(uri);
		if (proxy == null)
			proxy = ProxyRoutingClient.get().lookupProxyBlocking();
		var cb = Ok.Clients.get(proxy).newBuilder();
		cb.cookieJar(CookieJars.create());
		cb.addInterceptor(UserAgentInterceptor.getBrowserDefault());
		cb.addInterceptor(new MetaRefreshRedirectInterceptor());
		File htmlFile = generateHtmlFile();
		try {
			try (var rctx = Ok.Calls.execute(cb.build(), uri); var fos = new FileOutputStream(htmlFile)) {
				Utils.Bits.copy(rctx.getResponse().body().byteStream(), fos);
			}
			return applyInternal(htmlFile.getAbsolutePath());
		} catch (Exception e) {
			throw Utils.Exceptions.as(e, IOException.class);
		} finally {
			htmlFile.delete();
		}
	}

	@Override
	public HtmlSummaryServiceResponse apply(String html) {
		return applyInternal(html);
	}

	@SuppressWarnings("deprecation")
	protected HtmlSummaryServiceResponse applyInternal(String input) {
		Validate.notBlank(input);
		var script = String.format(this.template, StringEscapeUtils.escapeEcmaScript(input));
		return eval(script, HtmlSummaryServiceResponse.class).block();
	}

	protected static File generateHtmlFile() {
		File htmlFile = Utils.Files.tempFile(THIS_CLASS, "html", Utils.Crypto.getSecureRandomString() + ".html");
		return htmlFile;
	}

	public static void main(String[] args) throws IOException {
		var summary = HtmlSummaryServiceImpl.INSTANCE;
		summary.apply(HttpClients.getDefault().get("https://en.wikipedia.org/wiki/Philadelphia").asString().getBody());
		for (int i = 0; i < 1; i++) {
			var sw = StopWatch.createStarted();
			var result = summary.apply(URI.create("https://en.wikipedia.org/wiki/Psychokinesis"), null);
			System.out.println(sw.getTime() + " - " + result);
		}
		System.exit(0);
	}
}
