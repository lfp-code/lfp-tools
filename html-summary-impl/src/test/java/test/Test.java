package test;

import com.lfp.joe.serial.Serials;
import com.lfp.proxy.routing.client.ProxyRoutingClient;

public class Test {

	public static void main(String[] args) {
		var proxy = ProxyRoutingClient.get().lookupProxyBlocking();
		System.out.println(Serials.Gsons.get().toJson(proxy));
	}
}
