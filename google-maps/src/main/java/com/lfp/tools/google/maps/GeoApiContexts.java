package com.lfp.tools.google.maps;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.Proxy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.Validate;

import com.google.maps.GeoApiContext;
import com.google.maps.GeoApiContext.RequestHandler;
import com.google.maps.GeoApiContext.RequestHandler.Builder;
import com.google.maps.OkHttpRequestHandler;
import com.google.maps.PlaceAutocompleteRequest.SessionToken;
import com.google.maps.PlacesApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.AutocompletePrediction;
import com.google.maps.model.PlaceAutocompleteType;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Executors;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.google.maps.config.GoogleMapsConfig;

import okhttp3.OkHttpClient;

public class GeoApiContexts {

	private static final MemoizedSupplier<GeoApiContext> GEO_API_CONTEXT_S = Utils.Functions.memoize(() -> {
		String apiKey = Configs.get(GoogleMapsConfig.class).apiKey();
		OkHttpRequestHandlerBuilder okHttpRequestHandlerBuilder = new OkHttpRequestHandlerBuilder(Ok.Clients.get(),
				CoreTasks.executor());
		GeoApiContext context = new GeoApiContext.Builder().apiKey(apiKey)
				.requestHandlerBuilder(okHttpRequestHandlerBuilder).build();
		return context;
	});

	public static GeoApiContext get() {
		return GEO_API_CONTEXT_S.get();
	}

	public static void main(String[] args) throws ApiException, InterruptedException, IOException {
		var context = GeoApiContexts.get();
		AutocompletePrediction[] results = PlacesApi.placeAutocomplete(context, "1135 parson curry", new SessionToken())
				.types(PlaceAutocompleteType.ADDRESS).await();
		System.out.println(Serials.Gsons.getPretty().toJson(results));
		var placeDetails = PlacesApi.placeDetails(context, results[0].placeId).await();
		System.out.println(Serials.Gsons.getPretty().toJson(GeoApis.toLocation(placeDetails)));
	}
}
