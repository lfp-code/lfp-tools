package com.lfp.tools.google.maps;

import java.util.Optional;
import java.util.function.Function;

import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;

import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.LatLng;
import com.google.maps.model.PlaceDetails;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.location.Location;
import com.lfp.joe.location.Location.Builder;
import com.lfp.joe.location.Locations;
import com.lfp.joe.utils.Utils;

public enum PlaceDetailsToLocation implements Function<PlaceDetails, Location> {
	INSTANCE;

	@Override
	public Location apply(PlaceDetails placeDetails) {
		if (placeDetails == null)
			return null;
		var locationB = Location.builder();
		LatLng latLng = Optional.ofNullable(placeDetails.geometry).map(v -> v.location).orElse(null);
		if (latLng != null) {
			locationB.latitude(latLng.lat);
			locationB.longitude(latLng.lng);
		}
		JodaBeans.copyToBuilder(AddressComponentsToLocation.INSTANCE.apply(placeDetails.addressComponents), locationB);
		return locationB.build();
	}

	void append(Builder locationB, MetaProperty<String> metaProperty, AddressComponent addrComponent,
			AddressComponentType... addressComponentTypes) {
		append(locationB, metaProperty, v -> v, addrComponent, addressComponentTypes);
	}

	<P> void append(Builder locationB, MetaProperty<P> metaProperty, Function<String, P> converter,
			AddressComponent addrComponent, AddressComponentType... addressComponentTypes) {
		if (addrComponent == null || addressComponentTypes == null || addressComponentTypes.length == 0)
			return;
		var value = addrComponent.longName;
		if (Utils.Strings.isBlank(value))
			value = addrComponent.shortName;
		if (Utils.Strings.isBlank(value))
			return;
		var match = Utils.Lots.stream(addrComponent.types).filter(v -> {
			for (var addressComponentType : addressComponentTypes) {
				if (addressComponentType == null)
					continue;
				if (addressComponentType.equals(v))
					return true;
			}
			return false;
		}).findFirst().isPresent();
		if (!match)
			return;
		var propertyValue = converter.apply(value);
		locationB.set(metaProperty, propertyValue);
	}
}
