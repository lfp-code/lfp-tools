package com.lfp.tools.google.maps;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.Proxy;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.Validate;

import com.google.maps.GeoApiContext;
import com.google.maps.OkHttpRequestHandler;
import com.google.maps.GeoApiContext.RequestHandler;
import com.google.maps.GeoApiContext.RequestHandler.Builder;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.threads.Executors;
import com.lfp.joe.utils.Utils;

import okhttp3.OkHttpClient;

public class OkHttpRequestHandlerBuilder implements GeoApiContext.RequestHandler.Builder {

	@SuppressWarnings("unchecked")
	private static MemoizedSupplier<Constructor<OkHttpRequestHandler>> CONSTRUCTOR_S = Utils.Functions.memoize(() -> {
		var ctors = JavaCode.Reflections.getAllConstructors(OkHttpRequestHandler.class, v -> {
			var parameterTypes = v.getParameterTypes();
			if (parameterTypes.length != 2)
				return false;
			if (!OkHttpClient.class.isAssignableFrom(parameterTypes[0]))
				return false;
			if (!ExecutorService.class.isAssignableFrom(parameterTypes[1]))
				return false;
			return true;
		});
		Validate.isTrue(ctors.size() == 1);
		var ctor = ctors.iterator().next();
		ctor.setAccessible(true);
		return ctor;
	});

	private final OkHttpRequestHandler requestHandler;

	public OkHttpRequestHandlerBuilder(OkHttpClient client, Executor executor) {
		Objects.requireNonNull(client);
		Objects.requireNonNull(executor);
		try {
			this.requestHandler = CONSTRUCTOR_S.get().newInstance(client, Executors.asExecutorService(executor));
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
	}
	

	@Override
	public Builder connectTimeout(long timeout, TimeUnit unit) {
		return this;
	}

	@Override
	public Builder readTimeout(long timeout, TimeUnit unit) {
		return this;
	}

	@Override
	public Builder writeTimeout(long timeout, TimeUnit unit) {
		return this;
	}

	@Override
	public Builder queriesPerSecond(int maxQps) {
		return this;
	}

	@Override
	public Builder proxy(Proxy proxy) {
		return this;
	}

	@Override
	public Builder proxyAuthentication(String proxyUserName, String proxyUserPassword) {
		return this;
	}

	@Override
	public RequestHandler build() {
		return requestHandler;
	}

}
