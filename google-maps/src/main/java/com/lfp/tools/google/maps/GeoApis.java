package com.lfp.tools.google.maps;

import com.google.maps.model.PlaceDetails;
import com.lfp.joe.location.Location;

public class GeoApis {

	public static Location toLocation(PlaceDetails placeDetails) {
		return PlaceDetailsToLocation.INSTANCE.apply(placeDetails);
	}

}
