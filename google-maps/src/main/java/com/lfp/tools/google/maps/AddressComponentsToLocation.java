package com.lfp.tools.google.maps;

import java.util.function.Function;

import org.joda.beans.MetaProperty;

import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.lfp.joe.location.Location;
import com.lfp.joe.location.Location.Builder;
import com.lfp.joe.location.Locations;
import com.lfp.joe.utils.Utils;

public enum AddressComponentsToLocation implements Function<AddressComponent[], Location> {
	INSTANCE;

	@Override
	public Location apply(AddressComponent[] addressComponents) {
		if (addressComponents == null)
			return null;
		var locationB = Location.builder();
		var meta = Location.meta();
		Utils.Lots.stream(addressComponents).forEach(addrComponent -> {
			append(locationB, meta.number(), v -> Utils.Strings.parseNumber(v).map(Number::longValue).orElse(null),
					addrComponent, AddressComponentType.STREET_NUMBER);
			append(locationB, meta.street(), addrComponent, AddressComponentType.ROUTE);
			append(locationB, meta.locality(), addrComponent, AddressComponentType.LOCALITY);
			append(locationB, meta.subRegion(), addrComponent, AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_3);
			append(locationB, meta.region(), addrComponent, AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1);
			append(locationB, meta.postalCode(), addrComponent, AddressComponentType.POSTAL_CODE);
			append(locationB, meta.countryCode(), v -> Locations.parseCountryCode(v).orElse(null), addrComponent,
					AddressComponentType.COUNTRY);
		});
		return locationB.build();
	}

	private void append(Builder locationB, MetaProperty<String> metaProperty, AddressComponent addrComponent,
			AddressComponentType... addressComponentTypes) {
		append(locationB, metaProperty, v -> v, addrComponent, addressComponentTypes);
	}

	private <P> void append(Builder locationB, MetaProperty<P> metaProperty, Function<String, P> converter,
			AddressComponent addrComponent, AddressComponentType... addressComponentTypes) {
		if (addrComponent == null || addressComponentTypes == null || addressComponentTypes.length == 0)
			return;
		var value = addrComponent.longName;
		if (Utils.Strings.isBlank(value))
			value = addrComponent.shortName;
		if (Utils.Strings.isBlank(value))
			return;
		var match = Utils.Lots.stream(addrComponent.types).filter(v -> {
			for (var addressComponentType : addressComponentTypes) {
				if (addressComponentType == null)
					continue;
				if (addressComponentType.equals(v))
					return true;
			}
			return false;
		}).findFirst().isPresent();
		if (!match)
			return;
		var propertyValue = converter.apply(value);
		locationB.set(metaProperty, propertyValue);
	}
}
