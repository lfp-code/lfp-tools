package com.lfp.tools.google.maps.config;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;

public interface GoogleMapsConfig extends Config {

	String apiKey();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
	}
}
