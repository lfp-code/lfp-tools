package com.lfp.tools.phone.sms.impl.verification.onlinesimru;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.Validate;

import com.google.gson.JsonElement;
import com.lfp.joe.core.function.Throws.ThrowingBiFunction;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.phone.sms.impl.verification.onlinesimru.config.OnlineSimRUConfig;

import io.mikael.urlbuilder.UrlBuilder;
import one.util.streamex.EntryStream;

public enum ApiFunction
		implements ThrowingBiFunction<String, EntryStream<String, ? extends Object>, JsonElement, IOException> {
	INSTANCE;

	private static final String API_KEY_PARAM = "apikey";

	@Override
	public JsonElement apply(String method, final EntryStream<String, ? extends Object> parameters) throws IOException {
		Validate.notBlank(method);
		String url = String.format("https://onlinesim.ru/api/%s.php", method);
		EntryStream<String, Object> estream;
		if (parameters == null)
			estream = EntryStream.empty();
		else
			estream = parameters.mapValues(v -> v);
		estream = estream.filterKeys(Utils.Strings::isNotBlank);
		estream = estream.filterKeys(v -> !API_KEY_PARAM.equalsIgnoreCase(v));
		estream = estream.append(API_KEY_PARAM, Configs.get(OnlineSimRUConfig.class).apiKey());
		var urlb = UrlBuilder.fromString(url);
		for (var ent : estream) {
			var key = ent.getKey();
			var value = Optional.ofNullable(ent.getValue()).map(Object::toString).orElse("");
			urlb = urlb.addParameter(key, value);
		}
		return execute(urlb.toUri());
	}

	@SuppressWarnings("deprecation")
	protected JsonElement execute(URI uri) throws IOException {
		uri = Objects.requireNonNull(uri);
		var rb = HttpRequests.request();
		rb.uri(uri);
		try {
			var response = HttpClients.send(rb);
			Validate.isTrue(StatusCodes.isSuccess(response.statusCode(), 2));
			try (var is = response.body(); var isr = new InputStreamReader(is, Utils.Bits.getDefaultCharset())) {
				return Serials.Gsons.getJsonParser().parse(isr);
			}
		} catch (Exception e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
	}
}
