package com.lfp.tools.phone.sms.impl.verification.onlinesimru;

import java.util.Optional;

import com.lfp.joe.phone.PhoneNumbers;
import com.lfp.tools.phone.sms.service.verification.VerificationUpdate;

import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.neovisionaries.i18n.CountryCode;

@SuppressWarnings("serial")
public class StateResponse extends Response {

	public long getTzid() {
		return this.getAsNumber("tzid").get().longValue();
	}

	public String getService() {
		return getAsString("service").get();
	}

	public Optional<PhoneNumber> getPhoneNumber() {
		return this.getAsString("number").flatMap(PhoneNumbers::tryParse);
	}

	public Optional<String> getMsg() {
		return this.getAsString("msg");
	}

	public Optional<CountryCode> getCountryCode() {
		var cid = this.getAsNumber("country").map(Number::longValue).orElse(null);
		return NumbersStatsCache.INSTANCE.getCountryCode(cid);
	}

	public VerificationUpdate toVerificationUpdate() {
		this.validate("TZ_INPOOL", "TZ_NUM_WAIT", "TZ_NUM_ANSWER", "TZ_OVER_OK");
		boolean complete = "TZ_OVER_OK".equals(getResponse());
		var result = VerificationUpdate.builder().operationId(getTzid()).complete(complete).service(getService())
				.phoneNumber(getPhoneNumber().orElse(null)).message(getMsg().orElse(null))
				.countryCode(getCountryCode().orElse(null)).build();
		return result;
	}
}
