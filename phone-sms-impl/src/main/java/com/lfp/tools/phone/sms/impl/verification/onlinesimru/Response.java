package com.lfp.tools.phone.sms.impl.verification.onlinesimru;

import java.util.HashMap;
import java.util.Objects;
import java.util.Optional;

import com.lfp.joe.utils.Utils;

import org.apache.commons.lang3.Validate;

@SuppressWarnings("serial")
public class Response extends HashMap<String, Object> {

	protected Optional<Number> getAsNumber(String key) {
		var value = this.get(key);
		if (value == null)
			return Optional.empty();
		if (value instanceof Number)
			return Optional.of((Number) value);
		return Utils.Strings.parseNumber(value.toString());
	}

	protected Optional<String> getAsString(String key) {
		var value = this.get(key);
		if (value == null)
			return Optional.empty();
		var valueStr = value.toString();
		if (Utils.Strings.isBlank(valueStr))
			return Optional.empty();
		return Optional.of(valueStr);
	}

	public Object getResponse() {
		return get("response");
	}

	public void validate(String... validResponses) throws IllegalArgumentException {
		if (Objects.equals(1, getAsNumber("response").orElse(null)))
			return;
		var response = getResponse();
		String responseStr = response == null ? null : response.toString();
		if (responseStr != null) {
			var validResponsesList = Utils.Lots.stream(validResponses).filter(Utils.Strings::isNotBlank).distinct()
					.toList();
			if (validResponsesList.contains(responseStr))
				return;
		}
		Validate.notBlank(responseStr, "invalid response:%s", response);
	}

}
