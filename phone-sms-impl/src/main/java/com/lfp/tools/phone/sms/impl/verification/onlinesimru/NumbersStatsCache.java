package com.lfp.tools.phone.sms.impl.verification.onlinesimru;

import java.io.IOException;
import java.time.Duration;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import org.apache.commons.lang3.Validate;
import org.redisson.api.RBucket;
import org.threadly.concurrent.future.ListenableFuture;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.neovisionaries.i18n.CountryCode;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public enum NumbersStatsCache implements Supplier<NumbersStats> {
	INSTANCE;

	private final Duration refreshDuration = Duration.ofSeconds(5);
	private final Duration expireDuration = Duration.ofHours(6);
	private final LoadingCache<Nada, Value> memoryCache = Caffeine.newBuilder().executor(Threads.Pools.centralPool())
			.refreshAfterWrite(refreshDuration).build(nil -> {
				return loadNumbersStats();
			});
	private ListenableFuture<Value> updateFuture;

	@Override
	public NumbersStats get() {
		return memoryCache.get(Nada.get()).value;
	}

	public StreamEx<CountryStats> streamCountryStats() {
		return Utils.Lots.stream(get()).values();
	}

	public Optional<Long> getCountryId(CountryCode countryCode) {
		if (countryCode == null)
			return Optional.empty();
		var estream = streamCountryStats().mapToEntry(v -> v.getCode(), v -> v.getCountryCode().orElse(null));
		estream = estream.nonNullValues();
		estream = estream.filterValues(countryCode::equals);
		return estream.keys().findFirst();
	}

	public Optional<CountryCode> getCountryCode(Long id) {
		if (id == null)
			return Optional.empty();
		var estream = streamCountryStats().mapToEntry(v -> v.getCode(), v -> v.getCountryCode().orElse(null));
		estream = estream.nonNullValues();
		estream = estream.filterKeys(id::equals);
		return estream.values().findFirst();
	}

	private Value loadNumbersStats() throws Exception {
		var bucketName = RedisUtils.generateKey(streamKeyParts().append("bucket").toArray());
		RBucket<Value> bucket = TendisClients.getDefault().getBucket(bucketName, GsonCodec.getDefault());
		var lockAccessor = SemaphoreAccessor.create(11, Duration.ofSeconds(5),
				streamKeyParts().append("lock").toArray());
		return loadNumbersStats(bucket, lockAccessor);
	}

	private Value loadNumbersStats(RBucket<Value> bucket, SemaphoreAccessor lockAccessor) throws Exception {
		var value = bucket.get();
		if (!needsUpdate(value))
			return value;
		if (this.updateFuture == null || this.updateFuture.isDone())
			synchronized (this) {
				if (this.updateFuture == null || this.updateFuture.isDone()) {
					this.updateFuture = lockAccessor.accessAsync(() -> {
						var valueLock = bucket.get();
						if (!needsUpdate(valueLock))
							return valueLock;
						valueLock = loadNumbersStatsFresh();
						bucket.set(valueLock, expireDuration.toMillis(), TimeUnit.MILLISECONDS);
						return valueLock;
					}, Threads.Pools.centralPool());
				}
			}
		if (value == null)
			return this.updateFuture.get();
		Threads.Futures.logFailureError(updateFuture, true, "error during numbersStats update");
		return value;
	}

	private Value loadNumbersStatsFresh() throws IOException {
		var je = ApiFunction.INSTANCE.apply("getNumbersStats", EntryStream.of("country", "all"));
		var stats = Serials.Gsons.get().fromJson(je, NumbersStats.class);
		return new Value(stats);
	}

	private boolean needsUpdate(Value value) {
		if (value == null)
			return true;
		var elapsed = Duration.ofMillis(System.currentTimeMillis() - value.createdAt.getTime());
		return refreshDuration.minus(elapsed).isNegative();
	}

	private StreamEx<Object> streamKeyParts() {
		return StreamEx.of(this.getClass(), MachineConfig.isDeveloper(), 3);
	}

	private static class Value {

		public final Date createdAt = new Date();

		public final NumbersStats value;

		public Value(NumbersStats value) {
			Objects.requireNonNull(value);
			Validate.isTrue(value.size() > 0);
			this.value = value;
		}

	}

	public static void main(String[] args) {
		var numbersStats = NumbersStatsCache.INSTANCE.get();
		for (var ent : numbersStats.entrySet()) {
			var stream = StreamEx.of(ent.getKey(), ent.getValue().getName(),
					ent.getValue().getCountryCode().orElse(null));
			System.out.println(stream.joining("\t"));
		}
	}
}
