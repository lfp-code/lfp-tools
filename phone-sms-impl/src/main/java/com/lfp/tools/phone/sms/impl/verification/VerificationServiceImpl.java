package com.lfp.tools.phone.sms.impl.verification;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.reactor.Monos;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.tools.phone.sms.impl.verification.onlinesimru.ApiFunction;
import com.lfp.tools.phone.sms.impl.verification.onlinesimru.NumbersStatsCache;
import com.lfp.tools.phone.sms.impl.verification.onlinesimru.StateResponse;
import com.lfp.tools.phone.sms.service.verification.ServiceStats;
import com.lfp.tools.phone.sms.service.verification.VerificationService;
import com.lfp.tools.phone.sms.service.verification.VerificationUpdate;
import com.neovisionaries.i18n.CountryCode;

import io.mikael.urlbuilder.util.RuntimeURISyntaxException;
import one.util.streamex.EntryStream;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class VerificationServiceImpl implements VerificationService {

	public static final VerificationServiceImpl INSTANCE = new VerificationServiceImpl();

	protected VerificationServiceImpl() {

	}

	@Override
	public Flux<String> getServices() {
		var serviceStatsStreams = NumbersStatsCache.INSTANCE.streamCountryStats().map(v -> {
			return Utils.Lots.stream(v.getServices()).values();
		});
		var serviceStatsStream = Utils.Lots.flatMap(serviceStatsStreams);
		var slugStream = serviceStatsStream.map(v -> v.getSlug()).filter(Utils.Strings::isNotBlank).distinct();
		return Flux.fromStream(slugStream);
	}

	@Override
	public Mono<String> getServiceForName(String name) {
		var nameF = Utils.Strings.trimToNull(name);
		if (nameF == null)
			return Mono.empty();
		return getServices().filter(v -> {
			if (Utils.Strings.equalsIgnoreCase(nameF, v))
				return true;
			return false;
		}).next();
	}

	@Override
	public Flux<CountryCode> getCountryCodes() {
		var countryCodeStream = NumbersStatsCache.INSTANCE.streamCountryStats()
				.map(v -> v.getCountryCode().orElse(null)).nonNull().distinct();
		return Flux.fromStream(countryCodeStream);
	}

	@Override
	public Flux<Entry<CountryCode, List<ServiceStats>>> getServiceStats(CountryCode... countryCodeFilters) {
		var estream = NumbersStatsCache.INSTANCE.streamCountryStats().mapToEntry(v -> v.getCountryCode().orElse(null),
				v -> Utils.Lots.stream(v.getServices()).values().toList());
		estream = estream.nonNullKeys();
		var countryCodeFiltersList = Utils.Lots.stream(countryCodeFilters).nonNull().distinct().toList();
		estream = estream.filterKeys(v -> countryCodeFiltersList.isEmpty() || countryCodeFiltersList.contains(v));
		return Flux.fromStream(estream);
	}

	@Override
	public Flux<VerificationUpdate> getVerification(String service, CountryCode countryCode) {
		Mono<Long> operationIdMono = Monos.fromFuture(() -> {
			var future = Threads.Pools.centralPool().submit(() -> getNum(service, countryCode));
			return future;
		});
		operationIdMono = operationIdMono.cache();
		var tzidFlux = Flux.from(operationIdMono)
				.concatWith(operationIdMono.repeat().delayElements(Duration.ofSeconds(5)));
		var updateFlux = tzidFlux.map(tzid -> {
			var response = Utils.Functions.unchecked(() -> getState(tzid));
			return response.toVerificationUpdate();
		});
		return updateFlux;
	}

	@Override
	public Context getVerificationContext(String service, CountryCode countryCode) {
		var ctx = VerificationService.super.getVerificationContext(service, countryCode);
		ctx.onScrap(() -> {
			var tzid = ctx.getLastUpdate().map(VerificationUpdate::getOperationId).orElse(null);
			if (tzid == null)
				return;
			try {
				ApiFunction.INSTANCE.apply("setOperationOk", EntryStream.of("tzid", tzid));
			} catch (IOException e) {
				throw Utils.Exceptions.asRuntimeException(e);
			}
		});
		return ctx;
	}

	private long getNum(String service, CountryCode countryCode) throws IOException {
		Validate.notBlank(service);
		var params = EntryStream.of("service", service);
		var cidOp = NumbersStatsCache.INSTANCE.getCountryId(countryCode);
		if (cidOp.isPresent())
			params = params.append("country", cidOp.get() + "");
		var je = ApiFunction.INSTANCE.apply("getNum", params);
		var tzidJe = Serials.Gsons.tryGetAsPrimitive(je, "tzid").orElse(null);
		if (tzidJe == null)
			throw new IOException(String.format("unexpected response. service:%s countryCode:%s response:%s", service,
					countryCode, je));
		return tzidJe.getAsNumber().longValue();
	}

	private StateResponse getState(long tzid) throws IOException {
		var je = ApiFunction.INSTANCE.apply("getState", EntryStream.of("tzid", tzid + ""));
		if (je.isJsonArray() && je.getAsJsonArray().size() <= 1)
			je = Utils.Lots.stream(je.getAsJsonArray()).findFirst().orElse(null);
		var stateResponse = Serials.Gsons.get().fromJson(je, StateResponse.class);
		return stateResponse;
	}

	public static void main(String[] args)
			throws RuntimeURISyntaxException, IOException, InterruptedException, ExecutionException {
		var serviceName = "twitter";
		var impl = new VerificationServiceImpl();
		System.out.println(impl.getServiceForName(serviceName).block());
		var ctx = impl.getVerificationContext(serviceName, CountryCode.MX);

		System.out.println(ctx.getPhoneNumberFuture().get());
		System.out.println(ctx.getMessageFuture().get());

	}

}
