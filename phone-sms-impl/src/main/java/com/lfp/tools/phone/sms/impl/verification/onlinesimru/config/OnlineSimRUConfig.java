package com.lfp.tools.phone.sms.impl.verification.onlinesimru.config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;

import org.aeonbits.owner.Config;

public interface OnlineSimRUConfig extends Config {

	String apiKey();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
		Configs.printProperties(PrintOptions.json());
	}
}
