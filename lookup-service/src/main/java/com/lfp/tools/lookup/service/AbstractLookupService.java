package com.lfp.tools.lookup.service;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.apache.commons.lang3.Validate;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.ReadWriteAccessor;

public abstract class AbstractLookupService<X, C, Y> implements Function<X, Y> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private final ReadWriteAccessor<List<BiFunction<C, Duration, Optional<Y>>>> cacheReadFunctions = new ReadWriteAccessor<>(
			new ArrayList<>());
	private final Cache<String, Optional<Y>> memoryCache;
	private final Duration cacheDuration;

	public AbstractLookupService(long memoryCacheSize, Duration cacheDuration) {
		Validate.isTrue(cacheDuration != null && cacheDuration.getSeconds() > 0,
				"invalid cacheDuration:{}ms" + cacheDuration.toMillis());
		this.cacheDuration = cacheDuration;
		this.memoryCache = Caffeine.newBuilder().expireAfterWrite(this.cacheDuration).maximumSize(memoryCacheSize)
				.build();
	}

	@Override
	public Y apply(X input) {
		List<C> candidates = getCandidates(input);
		Objects.requireNonNull(candidates);
		for (var candidate : candidates)
			Objects.requireNonNull(candidate);
		List<Entry<C, Optional<Exception>>> failures = new ArrayList<>();
		for (C candidate : candidates) {
			try {
				Optional<Y> resultOp = lookup1stLevel(candidate);
				if (resultOp.isPresent())
					return resultOp.get();
				failures.add(Utils.Lots.entry(candidate, Optional.empty()));
			} catch (Exception e) {
				failures.add(Utils.Lots.entry(candidate, Optional.of(e)));
			}
		}
		onNotFound(input, failures);
		return null;
	}

	public Scrapable addCacheReadFunction(BiFunction<C, Duration, Optional<Y>> cacheReadFunction) {
		Objects.requireNonNull(cacheReadFunction);
		this.cacheReadFunctions.writeAccess(list -> {
			if (list.contains(cacheReadFunction))
				return;
			list.add(cacheReadFunction);
		});
		return Scrapable.create(() -> {
			this.cacheReadFunctions.writeAccess(list -> {
				list.removeIf(v -> v == cacheReadFunction);
			});
		});
	}

	protected Optional<Y> lookup1stLevel(C candidate) {
		String cachKey = getCacheKey(candidate, this.cacheDuration);
		return this.memoryCache.get(cachKey, nil -> lookup2ndLevel(candidate));

	}

	protected Optional<Y> lookup2ndLevel(C candidate) {
		Optional<Y> valueOp = null;
		var crfs = cacheReadFunctions.readAccess(list -> {
			return List.copyOf(list);
		});
		crfs = Utils.Lots.stream(crfs).append((c, d) -> cacheRead(c, d)).toList();
		for (var crf : crfs) {
			valueOp = crf.apply(candidate, this.cacheDuration);
			if (valueOp != null)
				return valueOp;
		}
		valueOp = lookupFresh(candidate);
		if (valueOp == null)
			return Optional.empty();
		this.cacheWrite(candidate, this.cacheDuration, valueOp);
		return valueOp;
	}

	protected void onNotFound(X input, List<Entry<C, Optional<Exception>>> failures) {
		List<String> messages = new ArrayList<>();
		messages.add(String.format("lookup failed:%s%s", input, failures.isEmpty() ? "" : " - "));
		for (var failureEnt : failures) {
			String message = failureEnt.getValue().map(v -> {
				var exceptionMsg = v.getMessage();
				if (Utils.Strings.isBlank(exceptionMsg))
					exceptionMsg = v.getClass().getSimpleName();
				return exceptionMsg;
			}).orElse("not found");
			messages.add(String.format("%s:'%s'", failureEnt.getKey(), message));
		}
		String logMessage = Utils.Lots.stream(messages).joining(" ");
		logger.warn(logMessage);
	}

	protected abstract List<C> getCandidates(X lookup);

	protected abstract String getCacheKey(C candidate, Duration cacheDuration);

	protected abstract Optional<Y> cacheRead(C candidate, Duration cacheDuration);

	protected abstract void cacheWrite(C candidate, Duration cacheDuration, Optional<Y> valueOp);

	protected abstract Optional<Y> lookupFresh(C candidate);

}
